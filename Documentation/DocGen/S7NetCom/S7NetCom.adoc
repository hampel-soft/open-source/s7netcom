//---------------------------------------------------------------------------
// TITLE AND SETTINGS
//---------------------------------------------------------------------------
include::S7NetCom-Title.adoc[]
include::../hse/hse-docgen-settings.adoc[]
include::S7NetCom-Abstract.adoc[]




//---------------------------------------------------------------------------
// GENERIC DESCRIPTION (STATIC)
//---------------------------------------------------------------------------
include::S7NetCom-Static.adoc[]



//---------------------------------------------------------------------------
// HSE STATE MACHINES
//---------------------------------------------------------------------------
== State Machines
include::S7NetCom-StateMachines.adoc[]



//---------------------------------------------------------------------------
// CALLING DEPENDENCY DIAGRAMS
//---------------------------------------------------------------------------
== Calling Dependency Diagrams
include::S7NetCom-Diagrams.adoc[leveloffset=+1]



//---------------------------------------------------------------------------
// APPENDICES
//---------------------------------------------------------------------------
[appendix]
include::S7NetCom-API-DQMH.adoc[]

[appendix]
include::S7NetCom-API-Libraries.adoc[]

[appendix]
include::S7NetCom-API-Classes.adoc[]



//---------------------------------------------------------------------------
// GLOSSARIES
//---------------------------------------------------------------------------
[glossary]
include::S7NetCom-Glossary.adoc[]


