﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="16008000">
	<Property Name="NI.Lib.Description" Type="Str">An open-source communication driver for talking to Siemens S7 PLCs via TCP/IP; implemented in plain LabVIEW TCP/IP primitives.

Copyright (c) 2019 Hampel Software Engineering (HSE), released under MIT license.
More info can be found on https://dokuwiki.hampel-soft.com/code/hse/s7com. 
Original code published by MarcoPolo5 on the NI Code Exchange forum.

____
Copyright (c) 2019 Hampel Software Engineering (HSE), released under BSD license. More info can be found on https://dokuwiki.hampel-soft.com/code/hse/s7netcom. Original code published by Marc Christenson, Sisu Devices.</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!**!!!*Q(C=\&gt;8.;6N"&amp;)&lt;B4S',G+T5A6%,JQ7ND,.5#[=&amp;&lt;&lt;051AW=%K)7N$%#\QYG"531#F3"90,/Z&amp;C*%RM2C-%'TWDU]]X@YXO(;WG5$^*\N8.FN^ONV_PD]@DRM@+AH^'D0&gt;&amp;@(&lt;`[WV`T\]04ZR`\NXL&gt;VY@LH_&lt;V_P0\0`E@\4^4^&amp;&lt;?SOMOH]]?]G=`[R0V"R%N;5&amp;TGKFPW:)]S:-]S:-]S9-]S)-]S)-]S*X=S:X=S:X=S9X=S)X=S)X=S%=F&amp;\H)21YJW4T:+&amp;EU73!:$%8*7_**0)EH]@"4C3@R**\%EXA9IM34?"*0YEE]4&amp;0C34S**`%E(J9;EBS6(%`C98E&amp;HM!4?!*0Y'&amp;,":Y!%'Q7,"QM!E.":`!F]!3?Q-.8":\!%XA#4_#B7Y%H]!3?Q".YG$+O3AR.L_2Y7%;/R`%Y(M@D?&amp;B;DM@R/"\(YXD94I\(]4A):U.H=1BS*DE$H"_/R`(Q)=@D?"S0YX%]&gt;)U\Z/0+&gt;%WPZ(A-D_%R0)&lt;(],#%$)`B-4S'R`#QL!S0Y4%]BM@QM*5-D_%R0!&lt;%W*4N:3RG4$1''9(BY46/CYW\&amp;%.CIUJV?&amp;7(5H896)&gt;)&gt;4B5.VVV-V5X388R62&gt;6&gt;&lt;&amp;5&amp;U(VR[H1+IRK%^8E0F!(XP?U,7V$7^'7N!6N4JP2JHXK@RZY/"SUX__VX7[VW7SU7KWU8#[V7#QUH]]VG]UUH5Z0DY&amp;,[OG"5-_F'UX;X@&gt;*_XJZU@,[5^N&gt;@7H@LGZ(XNPT`A^_!=^'P&gt;0P[X/.@A"&lt;D(SG!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">369131520</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.6</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.CoreWirePen" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!6!0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z1:7Y],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5T-DY.#DR/97VF0E:P=G6H=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP64-S0AU+0&amp;5T-DY.#DR/97VF0E*B9WNH=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0D%W.T=X-D%V0#^797Q_$1I],V5T-DY.#DR$&lt;(6T&gt;'6S0AU+0%ZB&lt;75_2GFM&lt;#"1982U:8*O0#^/97VF0AU+0%ZV&lt;56M&gt;(-_/$QP4H6N27RU=TY.#DR6/$Y.#DR/97VF0F*P&gt;S!Q0#^/97VF0AU+0&amp;:B&lt;$YR.T!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-4QP4G&amp;N:4Y.#DR797Q_/$5],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-DQP4G&amp;N:4Y.#DR797Q_-4=Q0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$-],UZB&lt;75_$1I]6G&amp;M0DAV0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$1],UZB&lt;75_$1I]6G&amp;M0D%X-$QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!V0#^/97VF0AU+0&amp;:B&lt;$YY.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!W0#^/97VF0AU+0&amp;:B&lt;$YR.T!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.TQP4G&amp;N:4Y.#DR797Q_/$5],V:B&lt;$Y.#DQP64A_$1I],U.M&gt;8.U:8)_$1I]34%W0AU+0%ZB&lt;75_6WFE&gt;'A],UZB&lt;75_$1I]6G&amp;M0D%],V:B&lt;$Y.#DQP34%W0AU+0%680AU+0%ZB&lt;75_47^E:4QP4G&amp;N:4Y.#DR$;'^J9W5_1W^Q?4QP1WBP;7.F0AU+0%.I&lt;WFD:4Z0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z&amp;?'.M&gt;8.J&gt;G5A4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_1GFU)%.M:7&amp;S0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"$&lt;X"Z0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X)A28BD&lt;(6T;8:F)%^S0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"#;81A1WRF98)],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;6TY.#DR&amp;4$Y.#DR/97VF0F.U?7RF0#^/97VF0AU+0%.I&lt;WFD:4Z4&lt;WRJ:$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I0#^$;'^J9W5_$1I]1WBP;7.F0E2P&gt;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I)%2P&gt;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I)%2P&gt;#"%&lt;X1],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DR&amp;4$Y.#DR/97VF0E:J&lt;'QA5H6M:4QP4G&amp;N:4Y.#DR$;'^J9W5_28:F&lt;C"0:'1],U.I&lt;WFD:4Y.#DR$;'^J9W5_6WFO:'FO:TQP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U6-0AU+0%6-0AU+0%ZB&lt;75_27ZE)%.B=(-],UZB&lt;75_$1I]1WBP;7.F0E2F:G&amp;V&lt;(1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2GRB&gt;$QP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U6-0AU+0#^$&lt;(6T&gt;'6S0AU+!!!!!!</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.EdgeWirePen" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!6)0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z1:7Y],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5T-DY.#DR/97VF0E:P=G6H=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0D-Z-T)R0#^797Q_$1I],V5T-DY.#DR6-T)_$1I]4G&amp;N:4Z#97.L:X*P&gt;7ZE)%.P&lt;'^S0#^/97VF0AU+0&amp;:B&lt;$YR.D=X.T)R.4QP6G&amp;M0AU+0#^6-T)_$1I]1WRV=X2F=DY.#DR/97VF0E:J&lt;'QA5'&amp;U&gt;'6S&lt;DQP4G&amp;N:4Y.#DR/&gt;7V&amp;&lt;(2T0DA],UZV&lt;56M&gt;(-_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-$QP4G&amp;N:4Y.#DR797Q_-D5V0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$%],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!S0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-TQP4G&amp;N:4Y.#DR797Q_-D5V0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$1],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!V0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.DQP4G&amp;N:4Y.#DR797Q_-D5V0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$=],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DQP1WRV=X2F=DY.#DR*-49_$1I]4G&amp;N:4Z8;72U;$QP4G&amp;N:4Y.#DR797Q_-TQP6G&amp;M0AU+0#^*-49_$1I]26=_$1I]4G&amp;N:4Z.&lt;W2F0#^/97VF0AU+0%.I&lt;WFD:4Z$&lt;X"Z0#^$;'^J9W5_$1I]1WBP;7.F0E^S0#^$;'^J9W5_$1I]1WBP;7.F0E6Y9WRV=WFW:3"0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z#;81A1WRF98)],U.I&lt;WFD:4Y.#DR$;'^J9W5_4G^U)%.P=(E],U.I&lt;WFD:4Y.#DR$;'^J9W5_4G^U)%^S0#^$;'^J9W5_$1I]1WBP;7.F0EZP=C"&amp;?'.M&gt;8.J&gt;G5A4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_4G^U)%*J&gt;#"$&lt;'6B=DQP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U680AU+0%6-0AU+0%ZB&lt;75_5X2Z&lt;'5],UZB&lt;75_$1I]1WBP;7.F0F.P&lt;'FE0#^$;'^J9W5_$1I]1WBP;7.F0E2B=WA],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'^U0#^$;'^J9W5_$1I]1WBP;7.F0E2B=WAA2'^U0#^$;'^J9W5_$1I]1WBP;7.F0E2B=WAA2'^U)%2P&gt;$QP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U6-0AU+0%6-0AU+0%ZB&lt;75_2GFM&lt;#"3&gt;7RF0#^/97VF0AU+0%.I&lt;WFD:4Z&amp;&gt;G6O)%^E:$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z8;7ZE;7ZH0#^$;'^J9W5_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP25Q_$1I]25Q_$1I]4G&amp;N:4Z&amp;&lt;G1A1W&amp;Q=TQP4G&amp;N:4Y.#DR$;'^J9W5_2'6G986M&gt;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z'&lt;'&amp;U0#^$;'^J9W5_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP25Q_$1I],U.M&gt;8.U:8)_$1I!!!!!</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!#XB5F.31QU+!!.-6E.$4%*76Q!!+8!!!!2R!!!!)!!!+6!!!!!6!!!!!2"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T!!!!!!!!I"9!A!!!-!!!#!!%!!!!!!1!!Q!]!,Q!(U#!!A!!!!!"!!%!"P````]!!!!!!!!!!!!!!!"2;LB$'=D_3[T;U:&gt;8';N_!!!!$!!!!"!!!!!!$A3?MWI-^%+O6H%5FF]U0^1&gt;D.G0!,)%[9!*G/TY1HY!!"!!!!!!!.1JQ(9?:-:0P]UV7T(79&amp;]"!!!!`````^1&gt;D.G0!,)%[9!*G/TY1HY!!!!1%6&gt;%"G)0%3AA``!&gt;)$!^XA!!!!1!!!!!!!!!K!!"4&amp;:$1Q!!!!)!!F:*4%)!!!!!5&amp;2)-!!!!!5!!1!"!!!!!!)!!F:*1U-!!!!!!2V4.UZF&gt;%.P&lt;6^$&lt;WZO:7.U;7^O5X2B&gt;(6T,G.U&lt;&amp;"53$!!!!!N!!%!"!!!#%.P&lt;H2S&lt;WRT(6-X4G6U1W^N8U.P&lt;GZF9X2J&lt;WZ4&gt;'&amp;U&gt;8-O9X2M!!!!!A!"`Q!!!!%!!1!!!!!!"A!!!!!!!!!!!!!!!!!!!!!!!Q!!!!)!!A!!!!!!*A!!!"ZYH'0A9'"O9,D!!-3-$EQ.8%!75Q+1^Y'"A3/!!1"J]!&lt;T!!!!!!!5!!!!%HC=9W"HY'&lt;A1))-!!+O!%-!!!"0!!!",HC=9W$!"0_"!%AR-D!Q@103L'DC9"L'JC&lt;!:3YOO[$CT%$-!H&gt;H'IA1\M"5$04.(C$."&amp;).V3E'E7*[!-1HU$8Q1_E,3')!Z5IK4!!!!!!-!!&amp;73524!!!!!!!$!!!"IQ!!!VBYH&amp;P!S-#1;7RB^A&amp;)-Q/R'%-$1X*_3CIP!Z$0!!&amp;PG"B)"A&amp;1`6JIYI9($K="A2[`@!O9X`S'J^N&amp;2;#Z2E7#K63EWU&gt;&amp;J..(B;742?8&amp;H````T=@Y4H=\:&amp;TX.%'J,;&lt;!SB_X%7&amp;!]1"UCQA_H^A"EA6KHESH5":)#W"J!&amp;O))L^!5"6(!U6SAQF,)9(IAY@&lt;T"BB$A5ZI1I&lt;/YFXPTG.RR!4QE=@-D3X;A"Z0&gt;/"*&amp;!):\/%![*YSY=/G*!0O-*E)'&gt;0$"@=]$^%Q9SI%2&amp;I.-%:"%,)]SC&lt;L&lt;D$BLA=(!1A6!:%+I#1B7!K"VA&amp;RTBC$M-$`_VL_`N9A83S&amp;&amp;K!-1A^8I-D!S-9$F'BL61/2MAGQEK"IN,%&amp;M"'ES-$0:Q0&lt;?B]BJ)ZLAQQP1AV&amp;5DW=M%.I/2Y1]$T$SA@6!^)$?R1=6]A7)(I/Q1)(M#F"U.:(_!MJ/!&lt;!%I/R0).G#%M0/A&lt;'&gt;`&amp;V@EN!B+V\!ULA,%S&lt;E&amp;"A:[V=%[J?%[J5YAF/GD5Z3GERJO9W1(&amp;ASP61;J+UAO)[#-!1#H&lt;K)$!!!!!6]!!!(U?*QT9'"AS$3W-'NA:'"A"G)RBA;'Z0S56!9EM)52Q1Y0;XYDU&amp;WC)N.:IM,48;/CU&amp;GDQN(.VMX@[;0#UMGC]O,0````GX]Q]E]ZQ.`['KCBVYWFUU7&amp;JT?=J4.%B3/NWY]FT&gt;K@B8`+1@\7CS"J$S;A4J\?)C;AA2R&gt;$ER74ERAP4N"EI[-9,VBD##^8:[-6H[-9*U,Q4I:)4I:14K\(2CNH3#W&gt;I*V1GQ.!^P;\=FC\1?SM\3UVY-&amp;IIM&amp;J#ONWQ(I(#?AV)(3_&amp;Z(*IA?*IB,0:H3L0W91,I]9;\U!F%A85!JC$OXH&gt;A"#BV_VY/PV@CX(8CNS,`NY'M:%%M=R")#M8B",!Y1CRH);DYC%"&gt;`'";?;V`@WQ5S!#G)'1S!7)F"!CT'"-3+3/)AY/TPYII?0S#V+E#=H&amp;NA9+"8(;R4'KZ4[A2#G4Y[27E[K?%W2H:AQ@";!#E&gt;AJU!!!!"&amp;A!!!=2YH$.A9'$).,9Q#W"E9'!'9D''"I&lt;E`*25"C41QIBABY=VPR(I,F'2[3R2Y?GO56(IL&amp;(B['9$ECS&gt;,#IP`PT``\`V!(`L/[$38D?74B=6HFZX%-72VOX'EN&lt;JTN).%P*A[P1";P&gt;2Y?A.:/I-5?(I=G$K='3#S$%CZ"AB=IQ&gt;DIT9Z&lt;I&gt;'$NB=CQ)/2;)(%OH)QN1A+@8%?Q3I$+/XA#Q8&amp;KX*UP;,E;'N/;@4'H]4B@4/A.QO#SNWY%JL20G.FSSBA?;DQD%R2^G70P[XCZ1=#%&amp;'9-"%'="25"C4%!MDS1/!M\_,K\IY1V3KQT%S18*:8L6Q4KFY4KF4C#5[;.4F+;4'GZD:!=7$+^F!!$SSGM@!!!!!!!-&amp;A#!*!!!"$%W,D!!!!!!$"9!A!!!!!1R.CYQ!!!!!!Q7!)!E!!!%-49O-!!!!!!-&amp;A#!!!!!"$%W,D!!!!!!$"9!A#1!!!1R.CYQ!!!!!"1"!!!!^6=VAHEGIIQO=V*/"EEZH1!!!!U!!!!!!!!!!!!!!!!!!!!!!!!!A0````_!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9"A!!'"_!!"B`Y!!:``A!'``]!"P``!!&lt;``Q!'``]!"P``!!&lt;``Q!'``]!"P``!!&lt;``Q!'``]!"P``!!:``A!'(`A!"A@A!!9"A!!(`````!!!%!0```````````````````````````````````````````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!0``QM,``Q!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!0``QM,#QM,#``]!!!!!!!!!!!!!!!!!!!!!``]!!0``QM,#QM,#QM,#QP``!!!!!!!!!!!!!!!!!!$``Q$`QM,#QM,#QM,#QM,#QM,`!!!!!!!!!!!!!!!!!0``!0```],#QM,#QM,#QM,```]!!!!!!!!!!!!!!!!!``]!`YS-```#QM,#QM,```T]`Q!!!!!!!!!!!!!!!!$``Q$`D)S-D0``QM,```T]`0T`!!!!!!!!!!!!!!!!!0``!0_-D)S-D)T```T]`0T]`0]!!!!!!!!!!!!!!!!!``]!`YS-D)S-D)T``0T]`0T]`Q!!!!!!!!!!!!!!!!$``Q$`D)S-D)S-D0`]`0T]`0T`!!!!!!!!!!!!!!!!!0``!0_-D)S-D)S-``T]`0T]`0]!!!!!!!!!!!!!!!!!``]!`YS-D)S-D)T``0T]`0T]`Q!!!!!!!!!!!!!!!!$``Q$`D)S-D)S-D0`]`0T]`0T`!!!!!!!!!!!!!!!!!0``!0_-D)S-D)S-``T]`0T]`0]!!!!!!!!!!!!!!!!!``]!!0``D)S-D)T``0T]`0``!!!!!!!!!!!!!!!!!!$``Q!!!!$``YS-D0`]`0``!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!``_-````!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!$```````````````````````````````````````````]!!!!#!!)!!!!!!+!!!5:13&amp;!!!!!"!!*52%.$!!!!!2V4.UZF&gt;%.P&lt;6^$&lt;WZO:7.U;7^O5X2B&gt;(6T,G.U&lt;&amp;"53$!!!!!N!!%!"!!!#%.P&lt;H2S&lt;WRT(6-X4G6U1W^N8U.P&lt;GZF9X2J&lt;WZ4&gt;'&amp;U&gt;8-O9X2M!!!!!A!!`Q!!!!%!!1!!!!!!"A!!!!!!!!!!!!!!!!!!!!!!!!!"!!!!SF"53$!!!!!!!!!!!!!$!!!(2A!!(="YH-W:87Q5623!TZVOSWS\&gt;7=,B29I/^X-LCWBU*$Q9S-)\&gt;3%AL61'B13J/QO5I%O&gt;,?6H]J0MF3L6M2+%2-38_4"&amp;RZY]!54)Q7.K_(H1?8(L+"PG#!0"!*-VT.X&gt;H:G&gt;K?\9Q8;0NT=&lt;O\ZO?&gt;]Z^Q\-Q!6:\ES:A4W3U#Y?TB:*5&amp;B)%9!BGN930\Z_I"L)Y_!4#EH%CRDW\DLT!C:+9%D%0/RN7)``)/L%R=40?1S$(%X=7E"6Y\+#C6Q"G,48%V#H"./TR4[]V7N,KDA"MA)]ZLAPM]/2%.I%+*6]OCK)3.!R%K&lt;,?JJ;?]-2A8Z6XM.7UZ6WC8AR&amp;BRFR"`(D7C[?_I3O)E&gt;QGPKA25716HTJT2B&amp;S+E)_[M2BFS&amp;U!BC(/,$)F9KQC,-4H5*F#+E/=+*/U)\J("G8@:;&amp;UU=FCD%.2F.O&gt;X,)MK]J*S[D=L6OX5!\(J.R7#;9)]8KWH,U:O*:@P$L[.2!AU5MQ#W9.P]%GIIFP'"@=&gt;48,O;!3*:A/&lt;RXB7H(?+M(M;)T:"$&lt;6YD%9J-GQK=FY%:."8K&lt;*M)P^('D:'-S2$&gt;%W\+FIW.Y&gt;DA3\_.!7XL_^02TG&gt;X:V^,2(AHSA0&gt;+?G;=F9MS_3)["&lt;)QC!K6A)U?B8R`T%*Q[&gt;1L$A+-GOB2&amp;JQHRF*S,\G9A&amp;@?!&amp;H@:KB;`FT"_YI/V&lt;]IR^.9R+LFW3OZ'/G[62V*!ZU@I'+:D)RV`J/-'/B\53*_0J,OY^'U93&amp;`QZ%F@C!$O4S-&gt;$J.?,,,2K6WE#/F)\]8I$=$B,$+,5?;!A@2?N$/AWMF'_AO:J0?KNF+E$QU.'?1Q5X5JUGW%K+1H(C=?S[2`G@C4R/%L3LK&gt;3L2A/A+R!KC8H;N()^P!4_-`39X`_[:Q&amp;W%#`.E4Q"?!!DD8)L&lt;RL2V\AXR6`:Z)-&amp;SN:)4T+P7'CU'I)OCU.Z7E0S4Y)"#TW?=,]5)&gt;ZV0!#6Z9IE9BY;*2?0DQ)59"2]6QN")/WJCI)0^C;C-;]=[NX:+UZ*$A1[S+B5+=I9:K[1;&gt;[.]-=+$SMW@0IH)=87Z5R=CK]PBBNQ+B!Z@Q&gt;%GF0(+?:&amp;&lt;^L%&gt;7H\F54FCF0"K8WNW;.Q0I$7Y\4_&gt;."=J8D^W&lt;T&gt;;^W7TQJEC#D]29/3,F6*,`L:;%5P4G[L5&lt;[%VXXR@5GXT:B+TF1;VCDM\H[?:T&gt;@-;&lt;8\`MMYGIP^R7J@CE,:K7*[\3XW'\(N@"_^[%"_M+6*[6:[PD`(RY+N-_$TKUB&lt;M5^(9MA*A:07.K#-%#QX5%]?I,&lt;UG"`6-EHKW&gt;8MIQD?(ZFKBH23&lt;U)\N%(QQ0TPN=JHF*$Y1]&gt;9'5IC2ZT#J3YX!=YF\-".LZ^E$4ZQ+9HLA:[%XM]@OT@]!HH#:Q#NZ+(RKQ".8ZL'-WV_9%XB39A+]T22YW#"X?:FX5I:&gt;PB\7'&lt;N]R[B&gt;@JV&amp;XBWNQ6X&gt;Q5Z`U#LTM-O5?3@'/E?(N]2]?J@PSOTS(0IY=XS[@#3TS]_C72_8,N^L"LW=C+@9Z&gt;`*A,Y)NW_BSR]QA4\@(0ID7J=0I9Y4;6U?DI\;Z?&gt;&lt;\@*LWPX&lt;,"0`S40M]D!YE&lt;I]@$K2ODQ=?`:&gt;(I&lt;'W/8BO!HQ"3&lt;!"`!R3YDRMOY\W/0,](&amp;"JDV0J@UHF8&lt;RN,P*W/2$/:_FBDWOBF"H:^!@[1BV]KW2^EBX/$@T[S4Y':E(GRBX.[E8[_KE9[ES`&amp;6_R'MWF'(C,PL&amp;Q*3=T]_#"SS6XG`I2HG47HL+XC@"OV!'R^.+\]K6+W.`H#C7Y+L3WBW['M`(@5_&amp;1R9-W&lt;)9'JZ&lt;6NS-&gt;]JE(I)"?WKGV&gt;GVT'./L9RRK0LLG=@==SA`@2S//;T!'WE63*#R[&gt;J$&gt;&lt;)#;6K-&amp;@C\LA,8"J5+H+3P1.&gt;NX!(9&lt;WMCBX1CR3M6%&gt;&lt;8"Q6(N/-JL"V0*^$?O@4DK8P5YWG?B@&gt;+E^:W\!C'OC/7SK.H\#=4]^`P9G^0K+.J^Y1[GP;-Q^'U&gt;[R(UT[4I]G/B&gt;(((]2V@:5(%AHDD7SLBPQ-8.'9DPR&lt;IS*@HB0ZK+=FV'5.^WX0&amp;0@N%QLX(2-+^]ZRQ$UU6NRXGO"?;()47SX"2PE3A^=QR$RA@+'^;64%[SRU^=)6,@TS1+!L',:Q`=,YNBPCS]L'SD#_AEF]&lt;;FY`;#,X@?[_18&gt;`,QRDZM6/TVJ?;SC&gt;N$#%\'$U@-L.YIC8?&amp;/IX9W78ALH/VWJ?%23-.D-E:Z$G9R*RZ"`48BP(=@IR"3Z/P,3SZKE["2D0%UZ_&gt;1^A\5'$]FL43&amp;AWDP(57X&gt;#T&lt;G&gt;`1QK]*&lt;MG."OZTF2BD&amp;QBRAK&lt;IV4TR#/&lt;"$.8-8UW:XT.?54Y^W&gt;7CG1/FK;D]&lt;@IF![03L,]*P;K%R)%X)=RLM_\&lt;#8P^1H@KUSD&lt;+IC*#_OHUH_WS-0E^S$VFUW3;U3L"S1I&amp;2N+,H)L-!!/&gt;A8&lt;S*VE2AC29#J\ELUX`%PKK_LQKJ2E^"+J9@T#ZW[W\&amp;^+G#&lt;B!!!!!!!%!!!!?A!!!!1!!!!!!!!!$!!"1E2)5!!!!!!!!Q!!!')!!!"S?*RD9'$)%Z"A_M&gt;1^Z?"3?!LE#(^FY&amp;:U)`R.Q-$JZ`!93$.+#!*&amp;*&lt;^S]!OK!U7VD[CS]%!";JMD"S3()=&amp;/=!S(#U;$0```_@Y?O1;8-52(TB4::9]BQ1!&amp;'):!!!!!!!!"!!!!!=!!",@!!!!#!!!!#&amp;@&lt;GF@4'&amp;T&gt;%NO&lt;X&gt;O4X&gt;O;7ZH4&amp;:$&lt;'&amp;T=U.M&gt;8.U:8)!!!%]&amp;A#!!!!!!!%!#!!Q`````Q!"!!!!!!%A!!!!#A!51$$`````#EF1)%&amp;E:(*F=X-!!!N!"A!%5'^S&gt;!!!$U!&amp;!!B4&lt;'^U)%ZP,A!!$U!&amp;!!B397.L)%ZP,A!!$5!$!!&gt;5;7VF&lt;X6U!!Z!=!!&amp;"V2$5#"3:79!&lt;!$R!!!!!!!!!!)15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=RV4.UZF&gt;%.P&lt;6^$&lt;WZO:7.U;7^O5X2B&gt;(6T,G.U&lt;!!V1"9!!AV/&lt;X1A1W^O&lt;G6D&gt;'6E#5.P&lt;GZF9X2F:!!21W^O&lt;G6D&gt;'FP&lt;C"4&gt;'&amp;U&gt;8-!%U!'!!R4:8&amp;V:7ZD:3"/&lt;SY!!"&gt;!"A!15%26)&amp;.J?G5A+%*Z&gt;'6T+1!!+E"1!!E!!!!"!!)!!Q!%!!5!"A!(!!A15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=Q!!!1!*!!!!!!!!!!V/36^*9W^O272J&gt;'^S!!!.&gt;B9!A!!!!!!"!!Z!-0````]%2'&amp;U91!!!1!!!!!.6$%U-$%Y-$)X$1!!!!!"&amp;R6-&lt;W&amp;E)#9A67ZM&lt;W&amp;E,GRW9WRB=X-!!!%!!!!!!!E!!!UC!71":&amp;"53$!!!!!%!!!!!!!!!!!!!!!"!!!!!1].4'&amp;Z:8)O&lt;(:D&lt;'&amp;T=Q!!!1!!!!!!"Q!!$,9!!!!!!!!!!!!!$*Y!+!!!$*A!!!Q!!!!!!!!A!#!!'!!!!!!!````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```U!!!!!!!$```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```U!!!!!!!$```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```U!!!!!!!$```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```U!!!!!!!$```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```U!!!!!!!$```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```U!!!!!!!$```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```U!!!!!!!$```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```U!!!!!!!$```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```U!!!!!!!$```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```U!!!!!!!$```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```U!!!!!!!$```X```X```X```X```X```X```X```U!!!!!!!$```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```U!!!!!!!$```X```X```X```X```X```U!!!!!!!!!G:E!G:E!!!!!!!$```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```U!!!!!!!$```X```X```X```U!!!!!!!!!G:E!G:E!G:E!G:E!G:E!G:E!!!!!!!$```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```U!!!!!!!$```X```U!!!!!!!!!G:E!G:E!G:E!G:E!G:E!G:E!G:E!G:E!G:E!G:E!!!!!!!$```X```X```X```X```X```X```X```X```X```X```X```X```X```U!!!!!!!$```U!!!!!G:E!G:E!G:E!G:E!G:E!G:E!G:E!G:E!G:E!G:E!G:E!G:E!G:E!G:E!!!$```X```X```X```X```X```X```X```X```X```X```X```X```U!!!!!!!$```U!!!!!!!!!!!!!G:E!G:E!G:E!G:E!G:E!G:E!G:E!G:E!G:E!G:E!!!!!!!!!!!$```X```X```X```X```X```X```X```X```X```X```X```X```U!!!!!!!$```U!!!"M!)"M!)!!!!!!!!!!G:E!G:E!G:E!G:E!G:E!G:E!!!!!!!".45V.45U!!!$```X```X```X```X```X```X```X```X```X```X```X```X```U!!!!!!!$```U!!!"M!)"M!)"M!)"M!)!!!!!!!!!!G:E!G:E!!!!!!!".45V.45V.45V.45U!!!$```X```X```X```X```X```X```X```X```X```X```X```X```U!!!!!!!$```U!!!"M!)"M!)"M!)"M!)"M!)"M!)!!!!!!!!".45V.45V.45V.45V.45V.45U!!!$```X```X```X```X```X```X```X```X```X```X```X```X```U!!!!!!!$```U!!!"M!)"M!)"M!)"M!)"M!)"M!)"M!)!!!!".45V.45V.45V.45V.45V.45U!!!$```X```X```X```X```X```X```X```X```X```X```X```X```U!!!!!!!$```U!!!"M!)"M!)"M!)"M!)"M!)"M!)"M!)!!!!".45V.45V.45V.45V.45V.45U!!!$```X```X```X```X```X```X```X```X```X```X```X```X```U!!!!!!!$```U!!!"M!)"M!)"M!)"M!)"M!)"M!)"M!)!!!!".45V.45V.45V.45V.45V.45U!!!$```X```X```X```X```X```X```X```X```X```X```X```X```U!!!!!!!$```U!!!"M!)"M!)"M!)"M!)"M!)"M!)"M!)!!!!".45V.45V.45V.45V.45V.45U!!!$```X```X```X```X```X```X```X```X```X```X```X```X```U!!!!!!!$```U!!!"M!)"M!)"M!)"M!)"M!)"M!)"M!)!!!!".45V.45V.45V.45V.45V.45U!!!$```X```X```X```X```X```X```X```X```X```X```X```X```U!!!!!!!$```U!!!"M!)"M!)"M!)"M!)"M!)"M!)"M!)!!!!".45V.45V.45V.45V.45V.45U!!!$```X```X```X```X```X```X```X```X```X```X```X```X```U!!!!!!!$```X```U!!!!!!!"M!)"M!)"M!)"M!)"M!)!!!!".45V.45V.45V.45U!!!!!!!$```X```X```X```X```X```X```X```X```X```X```X```X```X```U!!!!!!!$```X```X```X```U!!!!!!!"M!)"M!)"M!)!!!!".45V.45U!!!!!!!$```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```U!!!!!!!$```X```X```X```X```X```U!!!!!!!"M!)!!!!!!!!!!!!$```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```U!!!!!!!$```X```X```X```X```X```X```X```U!!!!!!!$```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```X```U!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````Q!!!!2';7RM:!%!!A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#V.N97RM)%:P&lt;H2T!!%)!1%!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962B9E^S:'6S!!!!329!A!!!!!!#!!5!"Q!!$!"!!!(`````!!!!!1!"!!!!#1!!!!!!!!!"!!!!!A!!!!-!!!!%!!!!"1!!!!9!!!!(!!!!#!!!!!!!!!!&lt;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'FN:8.U97VQ!!!!'29!A!!!!!!"!!5!"Q!!!1!!X,('*!!!!!!!!!!G4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B4'&amp;T&gt;%&amp;Q='RJ:725;7VF=X2B&lt;8!!!!!:&amp;A#!!!!!!!%!"1!(!!!"!!$=M=9E!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5?8"F2'6T9Q!!!4Q7!)!!!!!!!1!)!$$`````!!%!!!!!!3!!!!!+!"2!-0````]+36!A172E=G6T=Q!!#U!'!!21&lt;X*U!!!01!5!#&amp;.M&lt;X1A4G]O!!!01!5!#&amp;*B9WMA4G]O!!!.1!-!"V2J&lt;76P&gt;81!$E"Q!!5(6%.1)&amp;*F:A"M!0%!!!!!!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T(6-X4G6U1W^N8U.P&lt;GZF9X2J&lt;WZ4&gt;'&amp;U&gt;8-O9X2M!$6!&amp;A!#$5ZP&gt;#"$&lt;WZO:7.U:71*1W^O&lt;G6D&gt;'6E!"&amp;$&lt;WZO:7.U;7^O)&amp;.U982V=Q!41!9!$&amp;.F=86F&lt;G.F)%ZP,A!!&amp;U!'!""12&amp;5A5WF[:3!I1HFU:8-J!!!K1&amp;!!#1!!!!%!!A!$!!1!"1!'!!=!#""4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T!!!"!!E!!!!!!!!!(ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B5WF[:1!!!"E7!)!!!!!!!1!&amp;!!-!!!%!!!!!!"9!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B!!!"0B9!A!!!!!!+!"2!-0````]+36!A172E=G6T=Q!!#U!'!!21&lt;X*U!!!01!5!#&amp;.M&lt;X1A4G]O!!!01!5!#&amp;*B9WMA4G]O!!!.1!-!"V2J&lt;76P&gt;81!$E"Q!!5(6%.1)&amp;*F:A"M!0%!!!!!!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T(6-X4G6U1W^N8U.P&lt;GZF9X2J&lt;WZ4&gt;'&amp;U&gt;8-O9X2M!$6!&amp;A!#$5ZP&gt;#"$&lt;WZO:7.U:71*1W^O&lt;G6D&gt;'6E!"&amp;$&lt;WZO:7.U;7^O)&amp;.U982V=Q!41!9!$&amp;.F=86F&lt;G.F)%ZP,A!!&amp;U!'!""12&amp;5A5WF[:3!I1HFU:8-J!!!K1&amp;!!#1!!!!%!!A!$!!1!"1!'!!=!#""4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T!!!"!!E!!!!!!'9!!!!!%YA!!!!!!!!!!!!!!!!!!!!!!!!%!!Q!%A!!!!1!!!%3!!!!+!!!!!)!!!1!!!!!&amp;Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!("!!!$?XC=H6,.&lt;N.!%0Y=R`FREC9B*9%WI2PAU&amp;[C)F31/#U%)@5378&amp;[*J;^2B&amp;ONM4LC(,CF8A*HI+()%^1*FYX042#A2X*HJG&gt;G7__G18Q$"&amp;7U#@8=&amp;_0B"L+SU'U^#-PDHOXDI^$/:],8]XEX&amp;7?3O+"LS+=]4:SN:&amp;5,,M718GDI8G8QX13^PHJ$2X\X'&amp;PAW!BYBCI]!,SDFQII-YNF.S)[IXE)$0(HP^:GT6OIDC:81K:+/TR+VD&amp;S&gt;"B9R'C247KLPC3C,EP&gt;(3(8!XH`16T:^]%/XZXL52]!ESR_P8DZ`/N&lt;+M&lt;RZL='_[A$!-ZG-D$1I&amp;SCCDVBF%3+\&amp;A-G2J&amp;LN;T*;?%CTQF%&gt;R&amp;G!D!)T@^,=I7:NYC3L+P!ATD$[BR,_&lt;=KSU1]A1$5)TM9=['G93B#ABH:4',9$)AP``IMLL+NW5U!/UK#'&lt;GNH(1^*&lt;JPI;E/U1U3&lt;;J0FL"#,_YF_X__L?R"\P.,%=['/!/%]*/I]0V!T$9&gt;;ID3YV?SNW*P=^&gt;T?^6045$4T:UN=OG]R4V"(_.A)#!27OI)9/(O'!QBH[':#'[:/(%4&amp;[O64Q+:$3.6$Y!UN:NQE!!!!!!!"X!!%!!A!$!!5!!!"9!!]%!!!!!!]!W1$5!!!!91!0"!!!!!!0!.E!V!!!!'I!$Q1!!!!!$Q$:!.1!!!"TA!#%!)!!!!]!W1$5!!!!&gt;9!!B!#!!!!0!.E!V!B4:7&gt;P:3"631B4:7&gt;P:3"631B4:7&gt;P:3"631%S!4!!5F.31QU+!!.-6E.$4%*76Q!!+8!!!!2R!!!!)!!!+6!!!!!!!!!!!!!!!#!!!!!U!!!%:!!!!"R-35*/!!!!!!!!!7"-6F.3!!!!!!!!!8236&amp;.(!!!!!!!!!9B$1V.5!!!!!!!!!:R-38:J!!!!!!!!!&lt;"$4UZ1!!!!!!!!!=2544AQ!!!!!1!!!&gt;B%2E24!!!!!!!!!A"-372T!!!!!!!!!B2735.%!!!!!A!!!CBW:8*T!!!!"!!!!G241V.3!!!!!!!!!MB(1V"3!!!!!!!!!NR*1U^/!!!!!!!!!P"J9WQY!!!!!!!!!Q2$5%-S!!!!!!!!!RB-37:Q!!!!!!!!!SR'5%BC!!!!!!!!!U"'5&amp;.&amp;!!!!!!!!!V275%21!!!!!!!!!WB-37*E!!!!!!!!!XR#2%BC!!!!!!!!!Z"#2&amp;.&amp;!!!!!!!!![273624!!!!!!!!!\B%6%B1!!!!!!!!!]R.65F%!!!!!!!!!_")36.5!!!!!!!!!`271V21!!!!!!!!"!B'6%&amp;#!!!!!!!!""Q!!!!!`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"Q!!!!!!!!!!0````]!!!!!!!!!Q!!!!!!!!!!!`````Q!!!!!!!!$5!!!!!!!!!!$`````!!!!!!!!!.Q!!!!!!!!!!0````]!!!!!!!!"C!!!!!!!!!!!`````Q!!!!!!!!'1!!!!!!!!!!,`````!!!!!!!!!&lt;Q!!!!!!!!!!0````]!!!!!!!!"V!!!!!!!!!!!`````Q!!!!!!!!)I!!!!!!!!!!$`````!!!!!!!!!DA!!!!!!!!!!@````]!!!!!!!!$Y!!!!!!!!!!#`````Q!!!!!!!!6%!!!!!!!!!!4`````!!!!!!!!"G!!!!!!!!!!"`````]!!!!!!!!'=!!!!!!!!!!)`````Q!!!!!!!!;!!!!!!!!!!!H`````!!!!!!!!"J!!!!!!!!!!#P````]!!!!!!!!'I!!!!!!!!!!!`````Q!!!!!!!!;Q!!!!!!!!!!$`````!!!!!!!!"MA!!!!!!!!!!0````]!!!!!!!!'X!!!!!!!!!!!`````Q!!!!!!!!&gt;A!!!!!!!!!!$`````!!!!!!!!#W1!!!!!!!!!!0````]!!!!!!!!,&lt;!!!!!!!!!!!`````Q!!!!!!!!Q1!!!!!!!!!!$`````!!!!!!!!%VQ!!!!!!!!!!0````]!!!!!!!!4:!!!!!!!!!!!`````Q!!!!!!!".M!!!!!!!!!!$`````!!!!!!!!%XQ!!!!!!!!!!0````]!!!!!!!!4Z!!!!!!!!!!!`````Q!!!!!!!"0M!!!!!!!!!!$`````!!!!!!!!*N!!!!!!!!!!!0````]!!!!!!!!GW!!!!!!!!!!!`````Q!!!!!!!#&lt;A!!!!!!!!!!$`````!!!!!!!!*QQ!!!!!!!!!A0````]!!!!!!!!IV!!!!!!-5T&gt;/:82$&lt;WUO9X2M!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!2"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T!&amp;"53$!!!!!!!!!!!!!!!!!!%!!"!!!!!!!!!1!!!!%!&amp;%"1!!!.5T&gt;$4UUO&lt;(:D&lt;'&amp;T=Q!"!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!'!#!!!!!!!!!!!!!!!!!!!%!!!!!!!%"!!!!"Q!/1(!!"1&gt;51V!A5G6G!"2!-0````]+36!A172E=G6T=Q!!#U!$!!21&lt;X*U!!!.1!-!"V2J&lt;76P&gt;81!:A$R!!!!!!!!!!).5T&gt;$4UUO&lt;(:D&lt;'&amp;T=RJ4.U.P&lt;6^$&lt;WZO:7.U;7^O5X2B&gt;(6T,G.U&lt;!!V1"9!!AF$&lt;WZO:7.U:71.4G^U)%.P&lt;GZF9X2F:!!21W^O&lt;G6D&gt;'FP&lt;C"4&gt;'&amp;U&gt;8-!#U!&amp;!!24&lt;'^U!!"5!0(:LP*:!!!!!AV4.U.043ZM&gt;G.M98.T#6-X1U^.,G.U&lt;!!U1&amp;!!"A!!!!%!!A!$!!1!"2V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!9!!!!'````````````````````````````````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!9!)!!!!!!!!!!!!!!!!!!!1!!!!!!!A%!!!!)!!Z!=!!&amp;"V2$5#"3:79!&amp;%!Q`````QJ*5#"":'2S:8.T!!!,1!-!"&amp;"P=H1!!!V!!Q!(6'FN:7^V&gt;!"G!0%!!!!!!!!!!AV4.U.043ZM&gt;G.M98.T'F-X1W^N8U.P&lt;GZF9X2J&lt;WZ4&gt;'&amp;U&gt;8-O9X2M!$6!&amp;A!##5.P&lt;GZF9X2F:!V/&lt;X1A1W^O&lt;G6D&gt;'6E!"&amp;$&lt;WZO:7.U;7^O)&amp;.U982V=Q!,1!5!"&amp;.M&lt;X1!!".!"A!-5W6R&gt;76O9W5A4G]O!!"7!0(:M2A`!!!!!AV4.U.043ZM&gt;G.M98.T#6-X1U^.,G.U&lt;!!W1&amp;!!"Q!!!!%!!A!$!!1!"1!'(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!"Q!!!!=!!!!!!!!!!1!!!!)!!!!$!!!!"!!!!!8`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!5!)!!!!!!!!!!!!!!!!!!!1!!!!!!!!)!!!!)!!Z!=!!&amp;"V2$5#"3:79!&amp;%!Q`````QJ*5#"":'2S:8.T!!!,1!-!"&amp;"P=H1!!!V!!Q!(6'FN:7^V&gt;!"M!0%!!!!!!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T(6-X4G6U1W^N8U.P&lt;GZF9X2J&lt;WZ4&gt;'&amp;U&gt;8-O9X2M!$6!&amp;A!##5.P&lt;GZF9X2F:!V/&lt;X1A1W^O&lt;G6D&gt;'6E!"&amp;$&lt;WZO:7.U;7^O)&amp;.U982V=Q!21!5!#V*B9WMA*C"4&lt;'^U!".!"A!-5W6R&gt;76O9W5A4G]O!!"=!0(:P/5S!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T$&amp;-X4G6U1W^N,G.U&lt;!!W1&amp;!!"Q!!!!%!!A!$!!1!"1!'(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!"Q!!!!(````_!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;!#!!!!!!!!!!!!!!!!!!!%!!!!!!!%#!!!!#1!51$$`````#EF1)%&amp;E:(*F=X-!!!N!!Q!%5'^S&gt;!!!$5!$!!&gt;5;7VF&lt;X6U!!Z!=!!&amp;"V2$5#"3:79!&lt;!$R!!!!!!!!!!)15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=RV4.UZF&gt;%.P&lt;6^$&lt;WZO:7.U;7^O5X2B&gt;(6T,G.U&lt;!!V1"9!!AF$&lt;WZO:7.U:71.4G^U)%.P&lt;GZF9X2F:!!21W^O&lt;G6D&gt;'FP&lt;C"4&gt;'&amp;U&gt;8-!%U!'!!R4:8&amp;V:7ZD:3"/&lt;SY!!!^!"1!)5G&amp;D;S"/&lt;SY!!!^!"1!)5WRP&gt;#"/&lt;SY!!&amp;Y!]&gt;G]Z&gt;I!!!!#%&amp;-X4G6U1W^N,GRW9WRB=X--5T&gt;/:82$&lt;WUO9X2M!$B!5!!)!!!!!1!#!!-!"!!&amp;!!9!"RV$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!A!!!!)!!!!!1!!!!)!!!!$!!!!!!!!!!1!!!!'!!!!"@````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;!#!!!!!!!!!!!!!!!!!!!%!!!!!!!)#!!!!#1!51$$`````#EF1)%&amp;E:(*F=X-!!!N!!Q!%5'^S&gt;!!!$U!&amp;!!B397.L)%ZP,A!!$U!&amp;!!B4&lt;'^U)%ZP,A!!$5!$!!&gt;5;7VF&lt;X6U!!Z!=!!&amp;"V2$5#"3:79!&lt;!$R!!!!!!!!!!)15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=RV4.UZF&gt;%.P&lt;6^$&lt;WZO:7.U;7^O5X2B&gt;(6T,G.U&lt;!!V1"9!!AF$&lt;WZO:7.U:71.4G^U)%.P&lt;GZF9X2F:!!21W^O&lt;G6D&gt;'FP&lt;C"4&gt;'&amp;U&gt;8-!%U!'!!R4:8&amp;V:7ZD:3"/&lt;SY!!&amp;Y!]&gt;G]Z?-!!!!#%&amp;-X4G6U1W^N,GRW9WRB=X--5T&gt;/:82$&lt;WUO9X2M!$B!5!!)!!!!!1!#!!-!"!!&amp;!!9!"RV$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!A!!!!)!!!!!!!!!!%!!!!'!!!!"Q!!!!)!!!!$!!!!"!!!!!5!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;!#!!!!!!!!!!!!!!!!!!!%!!!!!!!-#!!!!#1!51$$`````#EF1)%&amp;E:(*F=X-!!!N!!Q!%5'^S&gt;!!!$U!&amp;!!B4&lt;'^U)%ZP,A!!$U!&amp;!!B397.L)%ZP,A!!$5!$!!&gt;5;7VF&lt;X6U!!Z!=!!&amp;"V2$5#"3:79!&lt;!$R!!!!!!!!!!)15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=RV4.UZF&gt;%.P&lt;6^$&lt;WZO:7.U;7^O5X2B&gt;(6T,G.U&lt;!!V1"9!!AF$&lt;WZO:7.U:71.4G^U)%.P&lt;GZF9X2F:!!21W^O&lt;G6D&gt;'FP&lt;C"4&gt;'&amp;U&gt;8-!%U!'!!R4:8&amp;V:7ZD:3"/&lt;SY!!&amp;Y!]&gt;G][ZM!!!!#%&amp;-X4G6U1W^N,GRW9WRB=X--5T&gt;/:82$&lt;WUO9X2M!$B!5!!)!!!!!1!#!!-!"!!&amp;!!9!"RV$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!A!!!!)!!!!!!!!!!%!!!!$!!!!!A!!!!1!!!!&amp;!!!!"A!!!!=!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;!#!!!!!!!!!!!!!!!!!!!%!!!!!!!1#!!!!#1!51$$`````#EF1)%&amp;E:(*F=X-!!!N!!Q!%5'^S&gt;!!!$U!&amp;!!B4&lt;'^U)%ZP,A!!$U!&amp;!!B397.L)%ZP,A!!$5!$!!&gt;5;7VF&lt;X6U!!Z!=!!&amp;"V2$5#"3:79!&lt;!$R!!!!!!!!!!)15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=RV4.UZF&gt;%.P&lt;6^$&lt;WZO:7.U;7^O5X2B&gt;(6T,G.U&lt;!!V1"9!!AF$&lt;WZO:7.U:71.4G^U)%.P&lt;GZF9X2F:!!21W^O&lt;G6D&gt;'FP&lt;C"4&gt;'&amp;U&gt;8-!%U!'!!R4:8&amp;V:7ZD:3"/&lt;SY!!&amp;Y!]&gt;G]]'Q!!!!#%&amp;-X4G6U1W^N,GRW9WRB=X--5T&gt;/:82$&lt;WUO9X2M!$B!5!!)!!!!!1!#!!-!"!!&amp;!!9!"RV$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!A!!!!)!!!!!!!!!!%!!!!#!!!!!Q!!!!1!!!!&amp;!!!!"A!!!!=!!!!!!!!!:A!!!!!4C!!!!!!!!1!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;!#!!!!!!!!!!!!!!!!!!!%!!!!!!!!$!!!!#1!51$$`````#EF1)%&amp;E:(*F=X-!!!N!!Q!%5'^S&gt;!!!$U!&amp;!!B4&lt;'^U)%ZP,A!!$U!&amp;!!B397.L)%ZP,A!!$5!$!!&gt;5;7VF&lt;X6U!!Z!=!!&amp;"V2$5#"3:79!&gt;!$R!!!!!!!!!!)95T&gt;/:82$&lt;WUA+'RP9W&amp;M+3ZM&gt;G.M98.T(6-X4G6U1W^N8U.P&lt;GZF9X2J&lt;WZ4&gt;'&amp;U&gt;8-O9X2M!$6!&amp;A!##5.P&lt;GZF9X2F:!V/&lt;X1A1W^O&lt;G6D&gt;'6E!"&amp;$&lt;WZO:7.U;7^O)&amp;.U982V=Q!41!9!$&amp;.F=86F&lt;G.F)%ZP,A!!&lt;A$RWDN-A!!!!!)95T&gt;/:82$&lt;WUA+'RP9W&amp;M+3ZM&gt;G.M98.T&amp;&amp;-X4G6U1W^N)#BM&lt;W.B&lt;#EO9X2M!$B!5!!)!!!!!1!#!!-!"!!&amp;!!9!"RV$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!A!!!!"`````A!!!!!!!!"G!!!!!"/)!!!!!!!"!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!9!)!!!!!!!!!!!!!!!!!!!1!!!!!!!!!!!!!*!"2!-0````]+36!A172E=G6T=Q!!#U!$!!21&lt;X*U!!!01!5!#&amp;.M&lt;X1A4G]O!!!01!5!#&amp;*B9WMA4G]O!!!.1!-!"V2J&lt;76P&gt;81!$E"Q!!5(6%.1)&amp;*F:A"M!0%!!!!!!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T(6-X4G6U1W^N8U.P&lt;GZF9X2J&lt;WZ4&gt;'&amp;U&gt;8-O9X2M!$6!&amp;A!##5.P&lt;GZF9X2F:!V/&lt;X1A1W^O&lt;G6D&gt;'6E!"&amp;$&lt;WZO:7.U;7^O)&amp;.U982V=Q!41!9!$&amp;.F=86F&lt;G.F)%ZP,A!!8A$RWDN&gt;PQ!!!!)15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=QR4.UZF&gt;%.P&lt;3ZD&gt;'Q!/%"1!!A!!!!"!!)!!Q!%!!5!"A!((5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!#!!!!!(````_!!!!!!!!!'9!!!!!%YA!!!!!!!%!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"A!A!!!!!!!!!!!!!!!!!!"!!!!!!!"!!!!!!E!&amp;%!Q`````QJ*5#"":'2S:8.T!!!,1!-!"&amp;"P=H1!!!^!"1!)5WRP&gt;#"/&lt;SY!!!^!"1!)5G&amp;D;S"/&lt;SY!!!V!!Q!(6'FN:7^V&gt;!!/1(!!"1&gt;51V!A5G6G!'Q!]1!!!!!!!!!#%&amp;-X4G6U1W^N,GRW9WRB=X-&gt;5T&gt;/:82$&lt;WV@1W^O&lt;G6D&gt;'FP&lt;F.U982V=SZD&gt;'Q!.5!7!!).4G^U)%.P&lt;GZF9X2F:!F$&lt;WZO:7.U:71!%5.P&lt;GZF9X2J&lt;WYA5X2B&gt;(6T!".!"A!-5W6R&gt;76O9W5A4G]O!!"?!0(=9\5D!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T$&amp;-X4G6U1W^N,G.U&lt;!!Y1&amp;!!#!!!!!%!!A!$!!1!"1!'!!=&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!)!!!!#!!!!!!!!!!"!!!!!A!!!!-!!!!%!!!!"@````]!!!!(!!!!!!!!!'9!!!!!%YA!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"9!A!!!!!!!!!!!!!!!!!!"!!!!!!!#!!!!!!I!&amp;%!Q`````QJ*5#"":'2S:8.T!!!,1!-!"&amp;"P=H1!!!^!"1!)5WRP&gt;#"/&lt;SY!!!^!"1!)5G&amp;D;S"/&lt;SY!!!V!!Q!(6'FN:7^V&gt;!!/1(!!"1&gt;51V!A5G6G!'Q!]1!!!!!!!!!#%&amp;-X4G6U1W^N,GRW9WRB=X-&gt;5T&gt;/:82$&lt;WV@1W^O&lt;G6D&gt;'FP&lt;F.U982V=SZD&gt;'Q!.5!7!!).4G^U)%.P&lt;GZF9X2F:!F$&lt;WZO:7.U:71!%5.P&lt;GZF9X2J&lt;WYA5X2B&gt;(6T!".!"A!-5W6R&gt;76O9W5A4G]O!!!01!-!#&amp;"%63"4;8JF!!"A!0(=L'0&amp;!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T$&amp;-X4G6U1W^N,G.U&lt;!![1&amp;!!#1!!!!%!!A!$!!1!"1!'!!=!#"V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!E!!!!*!!!!!!!!!!%!!!!#!!!!!Q!!!!1!!!!&amp;!!!!"A!!!!@`````!!!!!!!!!'9!!!!!%YA!!!!!!!!!!!!!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!7!)!!!!!!!!!!!!!!!!!!!1!!!!!!!Q!!!!!+!"2!-0````]+36!A172E=G6T=Q!!#U!$!!21&lt;X*U!!!01!5!#&amp;.M&lt;X1A4G]O!!!01!5!#&amp;*B9WMA4G]O!!!.1!-!"V2J&lt;76P&gt;81!$E"Q!!5(6%.1)&amp;*F:A"M!0%!!!!!!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T(6-X4G6U1W^N8U.P&lt;GZF9X2J&lt;WZ4&gt;'&amp;U&gt;8-O9X2M!$6!&amp;A!#$5ZP&gt;#"$&lt;WZO:7.U:71*1W^O&lt;G6D&gt;'6E!"&amp;$&lt;WZO:7.U;7^O)&amp;.U982V=Q!41!9!$&amp;.F=86F&lt;G.F)%ZP,A!!9!$R!!!!!!!!!!)15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=R.12&amp;5A5WF[:3!N,76O&gt;7UO9X2M!$.!&amp;A!$#4)U-#"#?82F=QEU/$!A1HFU:8-*/49Q)%*Z&gt;'6T!!B12&amp;5A5WF[:1!!9!$RX+RL+!!!!!)15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=QR4.UZF&gt;%.P&lt;3ZD&gt;'Q!/E"1!!E!!!!"!!)!!Q!%!!5!"A!(!!A&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!*!!!!#1!!!!!!!!!"!!!!!A!!!!-!!!!%!!!!"1!!!!9!!!!(`````Q!!!!!!!!"G!!!!!"/)!!!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"9!A!!!!!!!!!!!!!!!!!!"!!!!!!!%!!!!!!I!&amp;%!Q`````QJ*5#"":'2S:8.T!!!,1!-!"&amp;"P=H1!!!^!"1!)5WRP&gt;#"/&lt;SY!!!^!"1!)5G&amp;D;S"/&lt;SY!!!V!!Q!(6'FN:7^V&gt;!!/1(!!"1&gt;51V!A5G6G!'Q!]1!!!!!!!!!#%&amp;-X4G6U1W^N,GRW9WRB=X-&gt;5T&gt;/:82$&lt;WV@1W^O&lt;G6D&gt;'FP&lt;F.U982V=SZD&gt;'Q!.5!7!!).4G^U)%.P&lt;GZF9X2F:!F$&lt;WZO:7.U:71!%5.P&lt;GZF9X2J&lt;WYA5X2B&gt;(6T!".!"A!-5W6R&gt;76O9W5A4G]O!!"!!0%!!!!!!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T%V"%63"4;8JF)#UN=GFO:SZD&gt;'Q!%U!'!!B12&amp;5A5WF[:1!!9!$RX+S-+1!!!!)15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=QR4.UZF&gt;%.P&lt;3ZD&gt;'Q!/E"1!!E!!!!"!!)!!Q!%!!5!"A!(!!A&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!*!!!!#1!!!!!!!!!"!!!!!A!!!!-!!!!%!!!!"1!!!!9!!!!(!!!!#!!!!!!!!!"G!!!!!"/)!!!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"9!A!!!!!!!!!!!!!!!!!!"!!!!!!!&amp;!!!!!!I!&amp;%!Q`````QJ*5#"":'2S:8.T!!!,1!-!"&amp;"P=H1!!!^!"1!)5WRP&gt;#"/&lt;SY!!!^!"1!)5G&amp;D;S"/&lt;SY!!!V!!Q!(6'FN:7^V&gt;!!/1(!!"1&gt;51V!A5G6G!'Q!]1!!!!!!!!!#%&amp;-X4G6U1W^N,GRW9WRB=X-&gt;5T&gt;/:82$&lt;WV@1W^O&lt;G6D&gt;'FP&lt;F.U982V=SZD&gt;'Q!.5!7!!).4G^U)%.P&lt;GZF9X2F:!F$&lt;WZO:7.U:71!%5.P&lt;GZF9X2J&lt;WYA5X2B&gt;(6T!".!"A!-5W6R&gt;76O9W5A4G]O!!!81!9!%&amp;"%63"4;8JF)#B#?82F=SE!!'!!]&gt;SQ&lt;FU!!!!#%&amp;-X4G6U1W^N,GRW9WRB=X--5T&gt;/:82$&lt;WUO9X2M!$J!5!!*!!!!!1!#!!-!"!!&amp;!!9!"Q!)(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!#1!!!!E!!!!!!!!!!1!!!!)!!!!$!!!!"!!!!!5!!!!'!!!!"`````]!!!!!!!!!:A!!!!!4C!!!!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;A#!!!!!!!!!!!!!!!!!!!%!!!!!!!9!!!!!#A!51$$`````#EF1)%&amp;E:(*F=X-!!!N!"A!%5'^S&gt;!!!$U!&amp;!!B4&lt;'^U)%ZP,A!!$U!&amp;!!B397.L)%ZP,A!!$5!$!!&gt;5;7VF&lt;X6U!!Z!=!!&amp;"V2$5#"3:79!&lt;!$R!!!!!!!!!!)15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=RV4.UZF&gt;%.P&lt;6^$&lt;WZO:7.U;7^O5X2B&gt;(6T,G.U&lt;!!V1"9!!AV/&lt;X1A1W^O&lt;G6D&gt;'6E#5.P&lt;GZF9X2F:!!21W^O&lt;G6D&gt;'FP&lt;C"4&gt;'&amp;U&gt;8-!%U!'!!R4:8&amp;V:7ZD:3"/&lt;SY!!"&gt;!"A!15%26)&amp;.J?G5A+%*Z&gt;'6T+1!!9!$RX,('*!!!!!)15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=QR4.UZF&gt;%.P&lt;3ZD&gt;'Q!/E"1!!E!!!!"!!)!!Q!%!!5!"A!(!!A&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!*!!!!#1!!!!!!!!!"!!!!!A!!!!-!!!!%!!!!"1!!!!9!!!!(!!!!#!!!!!!!:A!!!!!4C!!!!!!!!!!!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!7!)!!!!!!!!!!!!!!!Q!!!!V4.U.043ZM&gt;G.M98.T!!!!%&amp;-X4G6U1W^N,GRW9WRB=X-!!!!95T&gt;/:82$&lt;WUA+'RP9W&amp;M+3ZM&gt;G.M98.T</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Property Name="NI_IconEditor" Type="Str">49 52 48 49 56 48 50 55 13 0 0 0 0 1 23 21 76 111 97 100 32 38 32 85 110 108 111 97 100 46 108 118 99 108 97 115 115 0 0 1 0 0 0 0 0 9 0 0 38 202 1 100 1 100 80 84 72 48 0 0 0 4 0 0 0 0 0 0 0 0 0 0 0 3 0 0 0 1 15 13 76 97 121 101 114 46 108 118 99 108 97 115 115 0 0 1 0 0 0 0 0 7 0 0 12 178 0 0 0 0 0 0 0 0 0 0 12 158 0 40 0 0 12 152 0 0 12 0 0 0 0 0 0 32 0 32 0 24 0 0 0 0 0 255 255 255 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 0 0 0 255 255 255 0 0 0 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 0 0 0 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 0 0 0 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 255 255 255 255 255 255 0 0 0 255 255 255 255 255 255 0 0 0 255 255 255 0 0 0 255 255 255 0 0 0 255 255 255 0 0 0 255 255 255 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 0 0 0 255 255 255 255 255 255 0 0 0 255 255 255 0 0 0 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 0 0 0 14 224 0 0 16 32 8 0 12 76 76 0 2 74 168 0 28 74 196 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 100 1 0 2 0 0 0 1 15 13 76 97 121 101 114 46 108 118 99 108 97 115 115 0 0 1 0 0 0 0 0 7 0 0 12 182 0 0 0 0 0 0 0 0 0 0 12 158 0 40 0 0 12 152 0 0 12 0 0 0 0 0 0 32 0 32 0 24 0 0 0 0 0 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 153 153 0 153 153 0 153 153 108 0 128 108 0 128 108 0 128 77 77 77 77 77 77 77 77 77 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 153 153 0 153 153 0 153 153 108 0 128 108 0 128 108 0 128 77 77 77 77 77 77 77 77 77 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 153 153 0 153 153 0 153 153 108 0 128 108 0 128 108 0 128 77 77 77 77 77 77 77 77 77 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 153 153 0 153 153 0 153 153 108 0 128 108 0 128 108 0 128 77 77 77 77 77 77 77 77 77 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 153 153 0 153 153 0 153 153 108 0 128 108 0 128 108 0 128 77 77 77 77 77 77 77 77 77 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 153 153 0 153 153 0 153 153 108 0 128 108 0 128 108 0 128 77 77 77 77 77 77 77 77 77 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 153 153 0 153 153 0 153 153 108 0 128 108 0 128 108 0 128 77 77 77 77 77 77 77 77 77 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 153 153 0 153 153 0 153 153 108 0 128 108 0 128 108 0 128 77 77 77 77 77 77 77 77 77 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 224 0 0 255 192 0 0 255 192 0 0 255 192 0 0 255 192 0 0 255 192 0 0 255 192 0 0 255 192 0 0 255 192 0 0 255 224 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 4 70 105 108 108 100 1 0 2 0 0 0 1 15 13 76 97 121 101 114 46 108 118 99 108 97 115 115 0 0 1 0 0 0 0 0 7 0 0 12 182 0 0 0 0 0 0 0 0 0 0 12 158 0 40 0 0 12 152 0 0 12 0 0 0 0 0 0 32 0 32 0 24 0 0 0 0 0 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 0 0 0 0 0 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 0 0 0 0 0 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 0 0 0 0 0 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 0 0 0 0 0 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 0 0 0 0 0 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 0 0 0 0 0 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 0 0 0 0 0 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 255 219 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 4 70 105 108 108 100 1 0 2 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 11 83 109 97 108 108 32 70 111 110 116 115 0 1 9 1 1

</Property>
	<Item Name="S7NetCom.ctl" Type="Class Private Data" URL="S7NetCom.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Accessors" Type="Folder">
		<Item Name="Connection Status" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Connection Status</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Connection Status</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Get Connection Status.vi" Type="VI" URL="../Accessors/Get Connection Status.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!')!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!'Q!]1!!!!!!!!!#%&amp;-X4G6U1W^N,GRW9WRB=X-&gt;5T&gt;/:82$&lt;WV@1W^O&lt;G6D&gt;'FP&lt;F.U982V=SZD&gt;'Q!.5!7!!).4G^U)%.P&lt;GZF9X2F:!F$&lt;WZO:7.U:71!%5.P&lt;GZF9X2J&lt;WYA5X2B&gt;(6T!#J!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!R4.UZF&gt;%.P&lt;3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!+%"Q!"Y!!")15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=Q!!#V-X4G6U1W^N)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">278934016</Property>
			</Item>
			<Item Name="Set Connection Status.vi" Type="VI" URL="../Accessors/Set Connection Status.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!')!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#J!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!R4.UZF&gt;%.P&lt;3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&lt;!$R!!!!!!!!!!)15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=RV4.UZF&gt;%.P&lt;6^$&lt;WZO:7.U;7^O5X2B&gt;(6T,G.U&lt;!!V1"9!!AV/&lt;X1A1W^O&lt;G6D&gt;'6E#5.P&lt;GZF9X2F:!!21W^O&lt;G6D&gt;'FP&lt;C"4&gt;'&amp;U&gt;8-!+%"Q!"Y!!")15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=Q!!#V-X4G6U1W^N)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">276836864</Property>
			</Item>
		</Item>
		<Item Name="IP Address" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">IP Address</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">IP Address</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Get IP Address.vi" Type="VI" URL="../Accessors/Get IP Address.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%Q!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"2!-0````]+36!A172E=G6T=Q!!+E"Q!"Y!!")15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=Q!!$&amp;-X4G6U1W^N)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!I1(!!(A!!%B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T!!!,5T&gt;/:82$&lt;WUA;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">276836864</Property>
			</Item>
		</Item>
		<Item Name="PDU Size (Bytes)" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">PDU Size (Bytes)</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">PDU Size (Bytes)</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Get PDU Size (Bytes).vi" Type="VI" URL="../Accessors/Get PDU Size (Bytes).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%T!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"&gt;!"A!15%26)&amp;.J?G5A+%*Z&gt;'6T+1!!+E"Q!"Y!!")15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=Q!!$&amp;-X4G6U1W^N)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!I1(!!(A!!%B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T!!!,5T&gt;/:82$&lt;WUA;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
		</Item>
		<Item Name="Rack Number" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Rack Number</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Rack Number</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Get Rack Number.vi" Type="VI" URL="../Accessors/Get Rack Number.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%L!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!"1!)5G&amp;D;S"/&lt;SY!!#J!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!R4.UZF&gt;%.P&lt;3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!+%"Q!"Y!!")15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=Q!!#V-X4G6U1W^N)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">276836864</Property>
			</Item>
		</Item>
		<Item Name="Slot Number" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Slot Number</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Slot Number</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Get Slot Number.vi" Type="VI" URL="../Accessors/Get Slot Number.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%L!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!"1!)5WRP&gt;#"/&lt;SY!!#J!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!R4.UZF&gt;%.P&lt;3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!+%"Q!"Y!!")15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=Q!!#V-X4G6U1W^N)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">276836864</Property>
			</Item>
		</Item>
		<Item Name="Get Sequence Number.vi" Type="VI" URL="../Accessors/Get Sequence Number.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$3!!!!"1!%!!!!%U!'!!R4:8&amp;V:7ZD:3"/&lt;SY!!#J!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!R4.UZF&gt;%.P&lt;3"P&gt;81!!#B!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!N4.UZF&gt;%.P&lt;3"J&lt;A"B!0!!$!!!!!!!!1!#!!!!!!!!!!!!!!!!!!!!!Q)!!(A!!!!!!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!%!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">276836864</Property>
		</Item>
		<Item Name="Set Sequence Number.vi" Type="VI" URL="../Accessors/Set Sequence Number.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$3!!!!"1!%!!!!+E"Q!"Y!!")15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=Q!!$&amp;-X4G6U1W^N)'^V&gt;!!!%U!'!!R4:8&amp;V:7ZD:3"/&lt;SY!!#B!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!N4.UZF&gt;%.P&lt;3"J&lt;A"B!0!!$!!!!!!!!!!"!!!!!!!!!!!!!!!!!!)!!Q)!!(A!!!!!!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!%!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">276836864</Property>
		</Item>
	</Item>
	<Item Name="API" Type="Folder">
		<Property Name="NI.SortType" Type="Int">0</Property>
		<Item Name="Type Specific Helpers" Type="Folder">
			<Property Name="NI.SortType" Type="Int">0</Property>
			<Item Name="Read Data - Boolean (Scalar).vi" Type="VI" URL="../API/Read Data - Boolean (Scalar).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(/!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"*!)1V#&lt;W^M:7&amp;O)&amp;:B&lt;(6F!#2!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!:4.S"P&gt;81!!".!"A!-2'&amp;U93"#&lt;'^D;S!D!!!21!5!#E*J&gt;#"0:G:T:81!!(5!]&gt;RE)I%!!!!#%&amp;-X4G6U1W^N,GRW9WRB=X-35T&gt;@476N&lt;X*Z8U&amp;S:7%O9X2M!%F!&amp;1!("F!A18*F91:*)%&amp;S:7%'4S""=G6B"EUA18*F91&gt;%1C""=G6B"U.P&gt;7ZU:8)&amp;6'FN:8)!#UVF&lt;7^S?3""=G6B!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%U!(!!V4&gt;'&amp;S&gt;#"":'2S:8.T!#*!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!64.S"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"Q!)!!E!#A!,!!1!$!-!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!#!!!!"!!!!!1!!!!#A!!!")!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!.!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710290</Property>
			</Item>
			<Item Name="Read Data - Byte (Array).vi" Type="VI" URL="../API/Read Data - Byte (Array).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(:!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!5!"1!!'%"!!!(`````!!5+1HFU:3""=H*B?1!!*%"Q!"Y!!")15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=Q!!"F-X)'^V&gt;!!!%U!'!!R%982B)%*M&lt;W.L)#-!!"&amp;!"A!,5G6B:#"-:7ZH&gt;'A!&gt;1$RX'1CA1!!!!)15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=R*4.V^.:7VP=HF@18*F93ZD&gt;'Q!35!6!!='5#""=G6B"EEA18*F91:0)%&amp;S:7%'43""=G6B"U2#)%&amp;S:7%(1W^V&lt;H2F=A65;7VF=A!,476N&lt;X*Z)%&amp;S:7%!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!41!=!$6.U98*U)%&amp;E:(*F=X-!)E"Q!"Y!!")15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=Q!!"6-X)'FO!'%!]!!-!!-!"!!'!!=!"!!)!!E!#A!,!!Q!"!!.!Q!!?!!!#1!!!!!!!!!*!!!!$1M!!!!!!!!)!!!!%A!!!"!!!!!)!!!!%A!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!Y!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Read Data - Byte (Scalar).vi" Type="VI" URL="../API/Read Data - Byte (Scalar).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!']!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"&amp;!"1!+1HFU:3"797RV:1!!*%"Q!"Y!!")15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=Q!!"F-X)'^V&gt;!!!%U!'!!R%982B)%*M&lt;W.L)#-!!(5!]&gt;RE)I%!!!!#%&amp;-X4G6U1W^N,GRW9WRB=X-35T&gt;@476N&lt;X*Z8U&amp;S:7%O9X2M!%F!&amp;1!("F!A18*F91:*)%&amp;S:7%'4S""=G6B"EUA18*F91&gt;%1C""=G6B"U.P&gt;7ZU:8)&amp;6'FN:8)!#UVF&lt;7^S?3""=G6B!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%U!(!!V4&gt;'&amp;S&gt;#"":'2S:8.T!#*!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!64.S"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"Q!%!!A!#1!+!!1!#Q-!!(A!!!E!!!!!!!!!#1!!!!U,!!!!!!!!#!!!!!!!!!!1!!!!#!!!!")!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821074</Property>
			</Item>
			<Item Name="Read Data - Char (Array).vi" Type="VI" URL="../API/Read Data - Char (Array).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(F!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"&amp;!!1!+1WBB=C"797RV:1!!'%"!!!(`````!!5+1WBB=C""=H*B?1!!*%"Q!"Y!!")15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=Q!!"F-X)'^V&gt;!!!%U!'!!R%982B)%*M&lt;W.L)#-!!"&amp;!"A!,5G6B:#"-:7ZH&gt;'A!&gt;1$RX'1CA1!!!!)15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=R*4.V^.:7VP=HF@18*F93ZD&gt;'Q!35!6!!='5#""=G6B"EEA18*F91:0)%&amp;S:7%'43""=G6B"U2#)%&amp;S:7%(1W^V&lt;H2F=A65;7VF=A!,476N&lt;X*Z)%&amp;S:7%!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!41!=!$6.U98*U)%&amp;E:(*F=X-!)E"Q!"Y!!")15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=Q!!"6-X)'FO!'%!]!!-!!-!"!!'!!=!"!!)!!E!#A!,!!Q!"!!.!Q!!?!!!#1!!!!!!!!!*!!!!$1M!!!!!!!!)!!!!%A!!!"!!!!!)!!!!%A!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!Y!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
			<Item Name="Read Data - Char (Scalar).vi" Type="VI" URL="../API/Read Data - Char (Scalar).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!']!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"&amp;!!1!+1WBB=C"797RV:1!!*%"Q!"Y!!")15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=Q!!"F-X)'^V&gt;!!!%U!'!!R%982B)%*M&lt;W.L)#-!!(5!]&gt;RE)I%!!!!#%&amp;-X4G6U1W^N,GRW9WRB=X-35T&gt;@476N&lt;X*Z8U&amp;S:7%O9X2M!%F!&amp;1!("F!A18*F91:*)%&amp;S:7%'4S""=G6B"EUA18*F91&gt;%1C""=G6B"U.P&gt;7ZU:8)&amp;6'FN:8)!#UVF&lt;7^S?3""=G6B!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%U!(!!V4&gt;'&amp;S&gt;#"":'2S:8.T!#*!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!64.S"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"Q!%!!A!#1!+!!1!#Q-!!(A!!!E!!!!!!!!!#1!!!!U,!!!!!!!!#!!!!!!!!!!1!!!!#!!!!")!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821074</Property>
			</Item>
			<Item Name="Read Data - DInt (Array).vi" Type="VI" URL="../API/Read Data - DInt (Array).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(&gt;!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!!Q!$34-S!"B!1!!"`````Q!&amp;#E2*&lt;H1A18*S98E!!#2!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!:4.S"P&gt;81!!".!"A!-2'&amp;U93"#&lt;'^D;S!D!!!21!9!#V*F971A4'6O:X2I!(5!]&gt;RE)I%!!!!#%&amp;-X4G6U1W^N,GRW9WRB=X-35T&gt;@476N&lt;X*Z8U&amp;S:7%O9X2M!%F!&amp;1!("F!A18*F91:*)%&amp;S:7%'4S""=G6B"EUA18*F91&gt;%1C""=G6B"U.P&gt;7ZU:8)&amp;6'FN:8)!#UVF&lt;7^S?3""=G6B!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%U!(!!V4&gt;'&amp;S&gt;#"":'2S:8.T!#*!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!64.S"J&lt;A"B!0!!$!!$!!1!"A!(!!1!#!!*!!I!#Q!-!!1!$1-!!(A!!!E!!!!!!!!!#1!!!!U,!!!!!!!!#!!!!")!!!!1!!!!#!!!!")!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!/!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
			<Item Name="Read Data - DInt (Scalar).vi" Type="VI" URL="../API/Read Data - DInt (Scalar).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!']!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"&amp;!!Q!+2%FO&gt;#"797RV:1!!*%"Q!"Y!!")15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=Q!!"F-X)'^V&gt;!!!%U!'!!R%982B)%*M&lt;W.L)#-!!(5!]&gt;RE)I%!!!!#%&amp;-X4G6U1W^N,GRW9WRB=X-35T&gt;@476N&lt;X*Z8U&amp;S:7%O9X2M!%F!&amp;1!("F!A18*F91:*)%&amp;S:7%'4S""=G6B"EUA18*F91&gt;%1C""=G6B"U.P&gt;7ZU:8)&amp;6'FN:8)!#UVF&lt;7^S?3""=G6B!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%U!(!!V4&gt;'&amp;S&gt;#"":'2S:8.T!#*!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!64.S"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"Q!%!!A!#1!+!!1!#Q-!!(A!!!E!!!!!!!!!#1!!!!U,!!!!!!!!#!!!!!!!!!!1!!!!#!!!!")!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821074</Property>
			</Item>
			<Item Name="Read Data - DTL (Scalar).vi" Type="VI" URL="../API/Read Data - DTL (Scalar).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*W!!!!&amp;A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!#U!'!!2::7&amp;S!!!,1!5!"5VP&lt;H2I!!F!"1!$2'&amp;Z!!N!"1!%3'^V=A!!$5!&amp;!!:.;7ZV&gt;'5!!!V!"1!'5W6D&lt;WZE!!!21!=!#EZB&lt;G^T:7.P&lt;G1!!!V!"1!(6W6F;W2B?1"6!0%!!!!!!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T'6-X1U^.43!N)%254#UN9WRV=X2F=CZD&gt;'Q!)E"1!!A!"!!&amp;!!9!"Q!)!!E!#A!,"F-X)%254!!!&amp;%"5!!9.4&amp;9A6'FN:3"4&gt;'&amp;N=!!E1(!!(A!!%B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T!!!'5T=A&lt;X6U!!!%!!!!%U!'!!R%982B)%*M&lt;W.L)#-!!(5!]&gt;RE)I%!!!!#%&amp;-X4G6U1W^N,GRW9WRB=X-35T&gt;@476N&lt;X*Z8U&amp;S:7%O9X2M!%F!&amp;1!("F!A18*F91:*)%&amp;S:7%'4S""=G6B"EUA18*F91&gt;%1C""=G6B"U.P&gt;7ZU:8)&amp;6'FN:8)!#UVF&lt;7^S?3""=G6B!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%U!(!!V4&gt;'&amp;S&gt;#"":'2S:8.T!#*!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!64.S"J&lt;A"B!0!!$!!$!!Q!$1!/!!]!%!!0!"%!%A!4!!]!&amp;!-!!(A!!!U)!!!*!!!!#1!!!!U,!!!!!!!!#!!!!!!!!!!1!!!!#A!!!"!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!6!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">17039744</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Read Data - DWord (Array).vi" Type="VI" URL="../API/Read Data - DWord (Array).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(&gt;!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"Q!$64-S!"B!1!!"`````Q!&amp;#U28&lt;X*E)%&amp;S=G&amp;Z!#2!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!:4.S"P&gt;81!!".!"A!-2'&amp;U93"#&lt;'^D;S!D!!!21!9!#V*F971A4'6O:X2I!(5!]&gt;RE)I%!!!!#%&amp;-X4G6U1W^N,GRW9WRB=X-35T&gt;@476N&lt;X*Z8U&amp;S:7%O9X2M!%F!&amp;1!("F!A18*F91:*)%&amp;S:7%'4S""=G6B"EUA18*F91&gt;%1C""=G6B"U.P&gt;7ZU:8)&amp;6'FN:8)!#UVF&lt;7^S?3""=G6B!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%U!(!!V4&gt;'&amp;S&gt;#"":'2S:8.T!#*!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!64.S"J&lt;A"B!0!!$!!$!!1!"A!(!!1!#!!*!!I!#Q!-!!1!$1-!!(A!!!E!!!!!!!!!#1!!!!U,!!!!!!!!#!!!!")!!!!1!!!!#!!!!")!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!/!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
			<Item Name="Read Data - DWord (Scalar).vi" Type="VI" URL="../API/Read Data - DWord (Scalar).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!']!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"&amp;!"Q!,2&amp;&gt;P=G1A6G&amp;M&gt;75!*%"Q!"Y!!")15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=Q!!"F-X)'^V&gt;!!!%U!'!!R%982B)%*M&lt;W.L)#-!!(5!]&gt;RE)I%!!!!#%&amp;-X4G6U1W^N,GRW9WRB=X-35T&gt;@476N&lt;X*Z8U&amp;S:7%O9X2M!%F!&amp;1!("F!A18*F91:*)%&amp;S:7%'4S""=G6B"EUA18*F91&gt;%1C""=G6B"U.P&gt;7ZU:8)&amp;6'FN:8)!#UVF&lt;7^S?3""=G6B!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%U!(!!V4&gt;'&amp;S&gt;#"":'2S:8.T!#*!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!64.S"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"Q!%!!A!#1!+!!1!#Q-!!(A!!!E!!!!!!!!!#1!!!!U,!!!!!!!!#!!!!!!!!!!1!!!!#!!!!")!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821074</Property>
			</Item>
			<Item Name="Read Data - Int (Array).vi" Type="VI" URL="../API/Read Data - Int (Array).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(&lt;!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!!A!$34%W!":!1!!"`````Q!&amp;#5FO&gt;#""=H*B?1!E1(!!(A!!%B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T!!!'5T=A&lt;X6U!!!41!9!$%2B&gt;'%A1GRP9WMA)Q!!%5!'!!N3:7&amp;E)%RF&lt;G&gt;U;!"V!0(=:#+"!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T%F-X8UVF&lt;7^S?6^"=G6B,G.U&lt;!"*1"5!"Q:1)%&amp;S:7%'33""=G6B"E]A18*F91:.)%&amp;S:7%(2%)A18*F91&gt;$&lt;X6O&gt;'6S"62J&lt;76S!!N.:7VP=HEA18*F91!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!".!"Q!.5X2B=H1A172E=G6T=Q!C1(!!(A!!%B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T!!!&amp;5T=A;7Y!91$Q!!Q!!Q!%!!9!"Q!%!!A!#1!+!!M!$!!%!!U$!!"Y!!!*!!!!!!!!!!E!!!!.#Q!!!!!!!!A!!!!3!!!!%!!!!!A!!!!3!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!$A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
			<Item Name="Read Data - Int (Scalar).vi" Type="VI" URL="../API/Read Data - Int (Scalar).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'[!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!!A!*37ZU)&amp;:B&lt;(6F!#2!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!:4.S"P&gt;81!!".!"A!-2'&amp;U93"#&lt;'^D;S!D!!"V!0(=:#+"!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T%F-X8UVF&lt;7^S?6^"=G6B,G.U&lt;!"*1"5!"Q:1)%&amp;S:7%'33""=G6B"E]A18*F91:.)%&amp;S:7%(2%)A18*F91&gt;$&lt;X6O&gt;'6S"62J&lt;76S!!N.:7VP=HEA18*F91!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!".!"Q!.5X2B=H1A172E=G6T=Q!C1(!!(A!!%B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T!!!&amp;5T=A;7Y!91$Q!!Q!!Q!%!!5!"A!%!!=!"!!)!!E!#A!%!!M$!!"Y!!!*!!!!!!!!!!E!!!!.#Q!!!!!!!!A!!!!!!!!!%!!!!!A!!!!3!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!$!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821074</Property>
			</Item>
			<Item Name="Read Data - Raw.vi" Type="VI" URL="../API/Read Data - Raw.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*&lt;!!!!%1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"1!&amp;!!!71%!!!@````]!"!F3:7&amp;E)%2B&gt;'%!%5!'!!N4:8&amp;V:7ZD:3"/&lt;Q!E1(!!(A!!%B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T!!!'5T=A&lt;X6U!!"T!0%!!!!!!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T&amp;&amp;-X8V:B=GFB9GRF8V2Z='5O9X2M!%6!&amp;1!*!SUN,1.#361%1FF5212$3%&amp;3"&amp;&gt;05E1$35Z5"5284V*%"%2*4F1%5E6"4!!!$6:B=GFB9GRF)&amp;2Z='5!%U!'!!R%982B)%*M&lt;W.L)#-!!"&amp;!"A!,5G6B:#"-:7ZH&gt;'A!&gt;1$RX'1CA1!!!!)15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=R*4.V^.:7VP=HF@18*F93ZD&gt;'Q!35!6!!='5#""=G6B"EEA18*F91:0)%&amp;S:7%'43""=G6B"U2#)%&amp;S:7%(1W^V&lt;H2F=A65;7VF=A!,476N&lt;X*Z)%&amp;S:7%!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!41!=!$6.U98*U)%&amp;E:(*F=X-!"!!!!#*!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!64.S"J&lt;A"B!0!!$!!$!!5!"A!(!!A!#1!+!!M!$!!.!!Y!$Q-!!(A!!!E!!!!*!!!!#1!!!!U,!!!1!!!!#!!!!")!!!!1!!!!#!!!!")!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!1!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
			</Item>
			<Item Name="Read Data - Real (Array).vi" Type="VI" URL="../API/Read Data - Real (Array).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(&gt;!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!#1!$5U&gt;-!"B!1!!"`````Q!&amp;#F*F97QA18*S98E!!#2!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!:4.S"P&gt;81!!".!"A!-2'&amp;U93"#&lt;'^D;S!D!!!21!9!#V*F971A4'6O:X2I!(5!]&gt;RE)I%!!!!#%&amp;-X4G6U1W^N,GRW9WRB=X-35T&gt;@476N&lt;X*Z8U&amp;S:7%O9X2M!%F!&amp;1!("F!A18*F91:*)%&amp;S:7%'4S""=G6B"EUA18*F91&gt;%1C""=G6B"U.P&gt;7ZU:8)&amp;6'FN:8)!#UVF&lt;7^S?3""=G6B!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%U!(!!V4&gt;'&amp;S&gt;#"":'2S:8.T!#*!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!64.S"J&lt;A"B!0!!$!!$!!1!"A!(!!1!#!!*!!I!#Q!-!!1!$1-!!(A!!!E!!!!!!!!!#1!!!!U,!!!!!!!!#!!!!")!!!!1!!!!#!!!!")!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!/!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Read Data - Real (Scalar).vi" Type="VI" URL="../API/Read Data - Real (Scalar).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!']!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"&amp;!#1!+5G6B&lt;#"797RV:1!!*%"Q!"Y!!")15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=Q!!"F-X)'^V&gt;!!!%U!'!!R%982B)%*M&lt;W.L)#-!!(5!]&gt;RE)I%!!!!#%&amp;-X4G6U1W^N,GRW9WRB=X-35T&gt;@476N&lt;X*Z8U&amp;S:7%O9X2M!%F!&amp;1!("F!A18*F91:*)%&amp;S:7%'4S""=G6B"EUA18*F91&gt;%1C""=G6B"U.P&gt;7ZU:8)&amp;6'FN:8)!#UVF&lt;7^S?3""=G6B!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%U!(!!V4&gt;'&amp;S&gt;#"":'2S:8.T!#*!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!64.S"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"Q!%!!A!#1!+!!1!#Q-!!(A!!!E!!!!!!!!!#1!!!!U,!!!!!!!!#!!!!!!!!!!1!!!!#!!!!")!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821074</Property>
			</Item>
			<Item Name="Read Data - Siemens Date&amp;Time (Scalar).vi" Type="VI" URL="../API/Read Data - Siemens Date&amp;Time (Scalar).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'^!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"*!6!!'#F2J&lt;75A5X2B&lt;8!!!#2!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!:4.S"P&gt;81!!".!"A!-2'&amp;U93"#&lt;'^D;S!D!!"V!0(=:#+"!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T%F-X8UVF&lt;7^S?6^"=G6B,G.U&lt;!"*1"5!"Q:1)%&amp;S:7%'33""=G6B"E]A18*F91:.)%&amp;S:7%(2%)A18*F91&gt;$&lt;X6O&gt;'6S"62J&lt;76S!!N.:7VP=HEA18*F91!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!".!"Q!.5X2B=H1A172E=G6T=Q!C1(!!(A!!%B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T!!!&amp;5T=A;7Y!91$Q!!Q!!Q!%!!5!"A!%!!=!"!!)!!E!#A!%!!M$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!A!!!!!!!!!%!!!!!I!!!!1!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!$!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">17039744</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Read Data - String.vi" Type="VI" URL="../API/Read Data - String.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!("!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!-0````]-5X2S;7ZH)&amp;:B&lt;(6F!!!E1(!!(A!!%B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T!!!'5T=A&lt;X6U!!!41!9!$%2B&gt;'%A1GRP9WMA)Q!!&gt;1$RX'1CA1!!!!)15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=R*4.V^.:7VP=HF@18*F93ZD&gt;'Q!35!6!!='5#""=G6B"EEA18*F91:0)%&amp;S:7%'43""=G6B"U2#)%&amp;S:7%(1W^V&lt;H2F=A65;7VF=A!,476N&lt;X*Z)%&amp;S:7%!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!41!=!$6.U98*U)%&amp;E:(*F=X-!)E"Q!"Y!!")15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=Q!!"6-X)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!(!!1!#!!*!!I!"!!,!Q!!?!!!#1!!!!!!!!!*!!!!$1M!!!!!!!!)!!!!!!!!!"!!!!!)!!!!%A!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!Q!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
			</Item>
			<Item Name="Read Data - Word (Array).vi" Type="VI" URL="../API/Read Data - Word (Array).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(&gt;!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"A!$64%W!"B!1!!"`````Q!&amp;#F&gt;P=G1A18*S98E!!#2!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!:4.S"P&gt;81!!".!"A!-2'&amp;U93"#&lt;'^D;S!D!!!21!9!#V*F971A4'6O:X2I!(5!]&gt;RE)I%!!!!#%&amp;-X4G6U1W^N,GRW9WRB=X-35T&gt;@476N&lt;X*Z8U&amp;S:7%O9X2M!%F!&amp;1!("F!A18*F91:*)%&amp;S:7%'4S""=G6B"EUA18*F91&gt;%1C""=G6B"U.P&gt;7ZU:8)&amp;6'FN:8)!#UVF&lt;7^S?3""=G6B!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%U!(!!V4&gt;'&amp;S&gt;#"":'2S:8.T!#*!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!64.S"J&lt;A"B!0!!$!!$!!1!"A!(!!1!#!!*!!I!#Q!-!!1!$1-!!(A!!!E!!!!!!!!!#1!!!!U,!!!!!!!!#!!!!")!!!!1!!!!#!!!!")!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!/!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
			<Item Name="Read Data - Word (Scalar).vi" Type="VI" URL="../API/Read Data - Word (Scalar).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!']!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"&amp;!"A!+6W^S:#"797RV:1!!*%"Q!"Y!!")15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=Q!!"F-X)'^V&gt;!!!%U!'!!R%982B)%*M&lt;W.L)#-!!(5!]&gt;RE)I%!!!!#%&amp;-X4G6U1W^N,GRW9WRB=X-35T&gt;@476N&lt;X*Z8U&amp;S:7%O9X2M!%F!&amp;1!("F!A18*F91:*)%&amp;S:7%'4S""=G6B"EUA18*F91&gt;%1C""=G6B"U.P&gt;7ZU:8)&amp;6'FN:8)!#UVF&lt;7^S?3""=G6B!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%U!(!!V4&gt;'&amp;S&gt;#"":'2S:8.T!#*!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!64.S"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"Q!%!!A!#1!+!!1!#Q-!!(A!!!E!!!!!!!!!#1!!!!U,!!!!!!!!#!!!!!!!!!!1!!!!#!!!!")!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821074</Property>
			</Item>
			<Item Name="Write Data - Boolean (Scalar).vi" Type="VI" URL="../API/Write Data - Boolean (Scalar).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!('!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#2!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!:4.S"P&gt;81!!".!"A!-2'&amp;U93"#&lt;'^D;S!D!!!21!5!#E*J&gt;#"0:G:T:81!!(5!]&gt;RE)I%!!!!#%&amp;-X4G6U1W^N,GRW9WRB=X-35T&gt;@476N&lt;X*Z8U&amp;S:7%O9X2M!%F!&amp;1!("F!A18*F91:*)%&amp;S:7%'4S""=G6B"EUA18*F91&gt;%1C""=G6B"U.P&gt;7ZU:8)&amp;6'FN:8)!#UVF&lt;7^S?3""=G6B!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%U!(!!V4&gt;'&amp;S&gt;#"":'2S:8.T!!J!)16797RV:1!C1(!!(A!!%B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T!!!&amp;5T=A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!9!"Q!)!!E!#A!,!!Q$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!A!!!!1!!!!%!!!!!I!!!!3!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!$1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Write Data - Byte (Array).vi" Type="VI" URL="../API/Write Data - Byte (Array).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(/!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#2!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!:4.S"P&gt;81!!".!"A!-2'&amp;U93"#&lt;'^D;S!D!!"V!0(=:#+"!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T%F-X8UVF&lt;7^S?6^"=G6B,G.U&lt;!"*1"5!"Q:1)%&amp;S:7%'33""=G6B"E]A18*F91:.)%&amp;S:7%(2%)A18*F91&gt;$&lt;X6O&gt;'6S"62J&lt;76S!!N.:7VP=HEA18*F91!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!".!"Q!.5X2B=H1A172E=G6T=Q!,1!5!"6:B&lt;(6F!"B!1!!"`````Q!+#E*Z&gt;'5A18*S98E!!#*!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!64.S"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"A!%!!=!#!!*!!M!$!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!#!!!!!!!!!!1!!!!#A!!!")!!!%3!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!.!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
			<Item Name="Write Data - Byte (Scalar).vi" Type="VI" URL="../API/Write Data - Byte (Scalar).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'W!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#2!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!:4.S"P&gt;81!!".!"A!-2'&amp;U93"#&lt;'^D;S!D!!"V!0(=:#+"!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T%F-X8UVF&lt;7^S?6^"=G6B,G.U&lt;!"*1"5!"Q:1)%&amp;S:7%'33""=G6B"E]A18*F91:.)%&amp;S:7%(2%)A18*F91&gt;$&lt;X6O&gt;'6S"62J&lt;76S!!N.:7VP=HEA18*F91!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!".!"Q!.5X2B=H1A172E=G6T=Q!,1!5!"6:B&lt;(6F!#*!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!64.S"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"A!%!!=!#!!*!!I!#Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!#!!!!!!!!!!1!!!!#A!!!")!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710290</Property>
			</Item>
			<Item Name="Write Data - Char (Array).vi" Type="VI" URL="../API/Write Data - Char (Array).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(/!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#2!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!:4.S"P&gt;81!!".!"A!-2'&amp;U93"#&lt;'^D;S!D!!"V!0(=:#+"!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T%F-X8UVF&lt;7^S?6^"=G6B,G.U&lt;!"*1"5!"Q:1)%&amp;S:7%'33""=G6B"E]A18*F91:.)%&amp;S:7%(2%)A18*F91&gt;$&lt;X6O&gt;'6S"62J&lt;76S!!N.:7VP=HEA18*F91!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!".!"Q!.5X2B=H1A172E=G6T=Q!,1!%!"6:B&lt;(6F!"B!1!!"`````Q!+#E.I98)A18*S98E!!#*!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!64.S"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"A!%!!=!#!!*!!M!$!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!#!!!!!!!!!!1!!!!#A!!!")!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!.!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Write Data - Char (Scalar).vi" Type="VI" URL="../API/Write Data - Char (Scalar).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'W!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#2!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!:4.S"P&gt;81!!".!"A!-2'&amp;U93"#&lt;'^D;S!D!!"V!0(=:#+"!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T%F-X8UVF&lt;7^S?6^"=G6B,G.U&lt;!"*1"5!"Q:1)%&amp;S:7%'33""=G6B"E]A18*F91:.)%&amp;S:7%(2%)A18*F91&gt;$&lt;X6O&gt;'6S"62J&lt;76S!!N.:7VP=HEA18*F91!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!".!"Q!.5X2B=H1A172E=G6T=Q!,1!%!"6:B&lt;(6F!#*!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!64.S"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"A!%!!=!#!!*!!I!#Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!#!!!!!!!!!!1!!!!#A!!!")!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Write Data - DInt (Array).vi" Type="VI" URL="../API/Write Data - DInt (Array).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(/!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#2!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!:4.S"P&gt;81!!".!"A!-2'&amp;U93"#&lt;'^D;S!D!!"V!0(=:#+"!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T%F-X8UVF&lt;7^S?6^"=G6B,G.U&lt;!"*1"5!"Q:1)%&amp;S:7%'33""=G6B"E]A18*F91:.)%&amp;S:7%(2%)A18*F91&gt;$&lt;X6O&gt;'6S"62J&lt;76S!!N.:7VP=HEA18*F91!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!".!"Q!.5X2B=H1A172E=G6T=Q!,1!-!"6:B&lt;(6F!"B!1!!"`````Q!+#E2*&lt;H1A18*S98E!!#*!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!64.S"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"A!%!!=!#!!*!!M!$!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!#!!!!!!!!!!1!!!!#A!!!")!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!.!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Write Data - DInt (Scalar).vi" Type="VI" URL="../API/Write Data - DInt (Scalar).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'W!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#2!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!:4.S"P&gt;81!!".!"A!-2'&amp;U93"#&lt;'^D;S!D!!"V!0(=:#+"!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T%F-X8UVF&lt;7^S?6^"=G6B,G.U&lt;!"*1"5!"Q:1)%&amp;S:7%'33""=G6B"E]A18*F91:.)%&amp;S:7%(2%)A18*F91&gt;$&lt;X6O&gt;'6S"62J&lt;76S!!N.:7VP=HEA18*F91!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!".!"Q!.5X2B=H1A172E=G6T=Q!,1!-!"6:B&lt;(6F!#*!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!64.S"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"A!%!!=!#!!*!!I!#Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!#!!!!!!!!!!1!!!!#A!!!")!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Write Data - DTL (Scalar).vi" Type="VI" URL="../API/Write Data - DTL (Scalar).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'X!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#2!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!:4.S"P&gt;81!!".!"A!-2'&amp;U93"#&lt;'^D;S!D!!"V!0(=:#+"!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T%F-X8UVF&lt;7^S?6^"=G6B,G.U&lt;!"*1"5!"Q:1)%&amp;S:7%'33""=G6B"E]A18*F91:.)%&amp;S:7%(2%)A18*F91&gt;$&lt;X6O&gt;'6S"62J&lt;76S!!N.:7VP=HEA18*F91!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!".!"Q!.5X2B=H1A172E=G6T=Q!-1&amp;1!"A6797RV:1!C1(!!(A!!%B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T!!!&amp;5T=A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!9!"!!(!!A!#1!+!!M$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!A!!!!!!!!!%!!!!!I!!!!3!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!$!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Write Data - DWord (Array).vi" Type="VI" URL="../API/Write Data - DWord (Array).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(/!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#2!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!:4.S"P&gt;81!!".!"A!-2'&amp;U93"#&lt;'^D;S!D!!"V!0(=:#+"!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T%F-X8UVF&lt;7^S?6^"=G6B,G.U&lt;!"*1"5!"Q:1)%&amp;S:7%'33""=G6B"E]A18*F91:.)%&amp;S:7%(2%)A18*F91&gt;$&lt;X6O&gt;'6S"62J&lt;76S!!N.:7VP=HEA18*F91!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!".!"Q!.5X2B=H1A172E=G6T=Q!,1!=!"6:B&lt;(6F!"B!1!!"`````Q!+#U28&lt;X*E)%&amp;S=G&amp;Z!#*!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!64.S"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"A!%!!=!#!!*!!M!$!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!#!!!!!!!!!!1!!!!#A!!!")!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!.!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Write Data - DWord (Scalar).vi" Type="VI" URL="../API/Write Data - DWord (Scalar).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'W!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#2!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!:4.S"P&gt;81!!".!"A!-2'&amp;U93"#&lt;'^D;S!D!!"V!0(=:#+"!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T%F-X8UVF&lt;7^S?6^"=G6B,G.U&lt;!"*1"5!"Q:1)%&amp;S:7%'33""=G6B"E]A18*F91:.)%&amp;S:7%(2%)A18*F91&gt;$&lt;X6O&gt;'6S"62J&lt;76S!!N.:7VP=HEA18*F91!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!".!"Q!.5X2B=H1A172E=G6T=Q!,1!=!"6:B&lt;(6F!#*!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!64.S"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"A!%!!=!#!!*!!I!#Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!#!!!!!!!!!!1!!!!#A!!!")!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Write Data - Int (Array).vi" Type="VI" URL="../API/Write Data - Int (Array).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(-!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#2!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!:4.S"P&gt;81!!".!"A!-2'&amp;U93"#&lt;'^D;S!D!!"V!0(=:#+"!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T%F-X8UVF&lt;7^S?6^"=G6B,G.U&lt;!"*1"5!"Q:1)%&amp;S:7%'33""=G6B"E]A18*F91:.)%&amp;S:7%(2%)A18*F91&gt;$&lt;X6O&gt;'6S"62J&lt;76S!!N.:7VP=HEA18*F91!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!".!"Q!.5X2B=H1A172E=G6T=Q!,1!)!"6:B&lt;(6F!":!1!!"`````Q!+#5FO&gt;#""=H*B?1!C1(!!(A!!%B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T!!!&amp;5T=A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!9!"!!(!!A!#1!,!!Q$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!A!!!!!!!!!%!!!!!I!!!!3!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!$1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Write Data - Int (Scalar).vi" Type="VI" URL="../API/Write Data - Int (Scalar).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'W!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#2!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!:4.S"P&gt;81!!".!"A!-2'&amp;U93"#&lt;'^D;S!D!!"V!0(=:#+"!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T%F-X8UVF&lt;7^S?6^"=G6B,G.U&lt;!"*1"5!"Q:1)%&amp;S:7%'33""=G6B"E]A18*F91:.)%&amp;S:7%(2%)A18*F91&gt;$&lt;X6O&gt;'6S"62J&lt;76S!!N.:7VP=HEA18*F91!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!".!"Q!.5X2B=H1A172E=G6T=Q!,1!)!"6:B&lt;(6F!#*!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!64.S"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"A!%!!=!#!!*!!I!#Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!#!!!!!!!!!!1!!!!#A!!!")!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Write Data - Raw.vi" Type="VI" URL="../API/Write Data - Raw.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)\!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#2!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!:4.S"P&gt;81!!(-!]1!!!!!!!!!#%&amp;-X4G6U1W^N,GRW9WRB=X-55T&gt;@6G&amp;S;7&amp;C&lt;'6@6(FQ:3ZD&gt;'Q!25!6!!E$,3UN!U**6!2#762&amp;"%.)16)%6U^32!.*4F1&amp;2&amp;&gt;05E1%2%F/6!2325&amp;-!!!.6G&amp;S;7&amp;C&lt;'5A6(FQ:1!41!9!$%2B&gt;'%A1GRP9WMA)Q!!&gt;1$RX'1CA1!!!!)15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=R*4.V^.:7VP=HF@18*F93ZD&gt;'Q!35!6!!='5#""=G6B"EEA18*F91:0)%&amp;S:7%'43""=G6B"U2#)%&amp;S:7%(1W^V&lt;H2F=A65;7VF=A!,476N&lt;X*Z)%&amp;S:7%!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!41!=!$6.U98*U)%&amp;E:(*F=X-!"1!&amp;!!!91%!!!@````]!#QJ8=GFU:3"%982B!!!C1(!!(A!!%B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T!!!&amp;5T=A;7Y!91$Q!!Q!!Q!%!!1!"1!'!!=!"!!)!!E!#A!-!!U$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!%!!!!!A!!!!!!!!!%!!!!!I!!!!3!!!"%A!!!")!!!U!!!!-!!!!!!!!!!!!!!%!$A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342714386</Property>
			</Item>
			<Item Name="Write Data - Real (Array).vi" Type="VI" URL="../API/Write Data - Real (Array).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(/!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#2!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!:4.S"P&gt;81!!".!"A!-2'&amp;U93"#&lt;'^D;S!D!!"V!0(=:#+"!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T%F-X8UVF&lt;7^S?6^"=G6B,G.U&lt;!"*1"5!"Q:1)%&amp;S:7%'33""=G6B"E]A18*F91:.)%&amp;S:7%(2%)A18*F91&gt;$&lt;X6O&gt;'6S"62J&lt;76S!!N.:7VP=HEA18*F91!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!".!"Q!.5X2B=H1A172E=G6T=Q!,1!E!"6:B&lt;(6F!"B!1!!"`````Q!+#F*F97QA18*S98E!!#*!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!64.S"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"A!%!!=!#!!*!!M!$!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!#!!!!!!!!!!1!!!!#A!!!")!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!.!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Write Data - Real (Scalar).vi" Type="VI" URL="../API/Write Data - Real (Scalar).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'W!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#2!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!:4.S"P&gt;81!!".!"A!-2'&amp;U93"#&lt;'^D;S!D!!"V!0(=:#+"!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T%F-X8UVF&lt;7^S?6^"=G6B,G.U&lt;!"*1"5!"Q:1)%&amp;S:7%'33""=G6B"E]A18*F91:.)%&amp;S:7%(2%)A18*F91&gt;$&lt;X6O&gt;'6S"62J&lt;76S!!N.:7VP=HEA18*F91!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!".!"Q!.5X2B=H1A172E=G6T=Q!,1!E!"6:B&lt;(6F!#*!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!64.S"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"A!%!!=!#!!*!!I!#Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!#!!!!!!!!!!1!!!!#A!!!")!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Write Data - Siemens Date &amp; Time (Scalar).vi" Type="VI" URL="../API/Write Data - Siemens Date &amp; Time (Scalar).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'X!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#2!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!:4.S"P&gt;81!!".!"A!-2'&amp;U93"#&lt;'^D;S!D!!"V!0(=:#+"!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T%F-X8UVF&lt;7^S?6^"=G6B,G.U&lt;!"*1"5!"Q:1)%&amp;S:7%'33""=G6B"E]A18*F91:.)%&amp;S:7%(2%)A18*F91&gt;$&lt;X6O&gt;'6S"62J&lt;76S!!N.:7VP=HEA18*F91!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!".!"Q!.5X2B=H1A172E=G6T=Q!-1&amp;1!"A6797RV:1!C1(!!(A!!%B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T!!!&amp;5T=A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!9!"!!(!!A!#1!+!!M$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!A!!!!!!!!!%!!!!!I!!!!3!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!$!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Write Data - String.vi" Type="VI" URL="../API/Write Data - String.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'\!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#2!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!:4.S"P&gt;81!!".!"A!-2'&amp;U93"#&lt;'^D;S!D!!"V!0(=:#+"!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T%F-X8UVF&lt;7^S?6^"=G6B,G.U&lt;!"*1"5!"Q:1)%&amp;S:7%'33""=G6B"E]A18*F91:.)%&amp;S:7%(2%)A18*F91&gt;$&lt;X6O&gt;'6S"62J&lt;76S!!N.:7VP=HEA18*F91!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!".!"Q!.5X2B=H1A172E=G6T=Q!11$$`````"F.U=GFO:Q!!)E"Q!"Y!!")15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=Q!!"6-X)'FO!'%!]!!-!!-!"!!%!!5!"!!'!!1!"Q!)!!E!#A!,!Q!!?!!!#1!!!!!!!!!!!!!!$1M!!!!!!!!)!!!!!!!!!"!!!!!)!!!!%A!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!Q!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
			</Item>
			<Item Name="Write Data - Word (Array).vi" Type="VI" URL="../API/Write Data - Word (Array).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(/!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#2!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!:4.S"P&gt;81!!".!"A!-2'&amp;U93"#&lt;'^D;S!D!!"V!0(=:#+"!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T%F-X8UVF&lt;7^S?6^"=G6B,G.U&lt;!"*1"5!"Q:1)%&amp;S:7%'33""=G6B"E]A18*F91:.)%&amp;S:7%(2%)A18*F91&gt;$&lt;X6O&gt;'6S"62J&lt;76S!!N.:7VP=HEA18*F91!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!".!"Q!.5X2B=H1A172E=G6T=Q!,1!9!"6:B&lt;(6F!"B!1!!"`````Q!+#F&gt;P=G1A18*S98E!!#*!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!64.S"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"A!%!!=!#!!*!!M!$!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!#!!!!!!!!!!1!!!!#A!!!")!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!.!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Write Data - Word (Scalar).vi" Type="VI" URL="../API/Write Data - Word (Scalar).vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'W!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#2!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!:4.S"P&gt;81!!".!"A!-2'&amp;U93"#&lt;'^D;S!D!!"V!0(=:#+"!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T%F-X8UVF&lt;7^S?6^"=G6B,G.U&lt;!"*1"5!"Q:1)%&amp;S:7%'33""=G6B"E]A18*F91:.)%&amp;S:7%(2%)A18*F91&gt;$&lt;X6O&gt;'6S"62J&lt;76S!!N.:7VP=HEA18*F91!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!".!"Q!.5X2B=H1A172E=G6T=Q!,1!9!"6:B&lt;(6F!#*!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!64.S"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"A!%!!=!#!!*!!I!#Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!#!!!!!!!!!!1!!!!#A!!!")!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
		</Item>
		<Item Name="Close Connection.vi" Type="VI" URL="../API/Close Connection.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%=!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#J!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!R4.UZF&gt;%.P&lt;3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!+%"Q!"Y!!")15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=Q!!#V-X4G6U1W^N)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
		<Item Name="Create Object.vi" Type="VI" URL="../API/Create Object.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;@!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#J!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!R4.UZF&gt;%.P&lt;3"P&gt;81!!!V!!Q!(6'FN:7^V&gt;!!,1!5!"&amp;*B9WM!!%!!]1!!!!!!!!!#%&amp;-X4G6U1W^N,GRW9WRB=X-45%26)&amp;.J?G5A,3VS;7ZH,G.U&lt;!!41!9!#&amp;"%63"4;8JF!!!71&amp;!!!Q!!!!%!!ABF=H*P=C"J&lt;A!!#U!&amp;!!24&lt;'^U!!!,1!9!"&amp;"P=H1!!"2!-0````]+36!A172E=G6T=Q!!6!$Q!!Q!!Q!%!!1!"1!'!!1!"Q!)!!E!#A!,!!Q#!!"Y!!!.#!!!!!!!!!!!!!!*!!!!#!!!!!!!!!!)!!!!!!!!!!A!!!!1!!!!%!!!!B!!!!!!!1!.!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
		</Item>
		<Item Name="Open Connection.vi" Type="VI" URL="../API/Open Connection.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%3!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#J!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!R4.UZF&gt;%.P&lt;3"P&gt;81!!":!5!!$!!!!!1!##'6S=G^S)'FO!!!I1(!!(A!!%B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T!!!,5T&gt;/:82$&lt;WUA;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342714386</Property>
		</Item>
		<Item Name="Read Array PLC Data.vi" Type="VI" URL="../API/Read Array PLC Data.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!1!!!!!1!)!0)S^#@P!!%!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
		</Item>
		<Item Name="Read Scalar PLC Data.vi" Type="VI" URL="../API/Read Scalar PLC Data.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!1!!!!!1!)!0*_(.&gt;F!!%!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
		</Item>
		<Item Name="Write Array PLC Data.vi" Type="VI" URL="../API/Write Array PLC Data.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!1!!!!!1!)!0)S^&amp;JU!!%!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
		</Item>
		<Item Name="Write Scalar PLC Data.vi" Type="VI" URL="../API/Write Scalar PLC Data.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!1!!!!!1!)!0*_(3]&gt;!!%!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
		</Item>
	</Item>
	<Item Name="Ctrls &amp; Types" Type="Folder">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="S7NetCom_ConnectionStatus.ctl" Type="VI" URL="../Controls/S7NetCom_ConnectionStatus.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"O!!!!!1"G!0%!!!!!!!!!!AV4.U.043ZM&gt;G.M98.T'F-X1W^N8U.P&lt;GZF9X2J&lt;WZ4&gt;'&amp;U&gt;8-O9X2M!$6!&amp;A!##5.P&lt;GZF9X2F:!V/&lt;X1A1W^O&lt;G6D&gt;'6E!"&amp;$&lt;WZO:7.U;7^O)&amp;.U982V=Q!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		</Item>
		<Item Name="S7_Memory_Area.ctl" Type="VI" URL="../Controls/S7_Memory_Area.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"/!!!!!1"'!0%!!!!!!!!!!AV4.U.043ZM&gt;G.M98.T&amp;F-X1W^N8U&amp;E:(*F=X.@18*F93ZD&gt;'Q!'5!&amp;!!^4.V^":'2S:8.T8U&amp;S:7%!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
		</Item>
		<Item Name="S7_Variable_Type.ctl" Type="VI" URL="../Controls/S7_Variable_Type.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"3!!!!!1"+!0%!!!!!!!!!!AV4.U.043ZM&gt;G.M98.T'&amp;-X1W^N8V2S97ZT='^S&gt;&amp;^4;8JF,G.U&lt;!!&lt;1!5!%6-X8V2S97ZT='^S&gt;&amp;^4;8JF!!%!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
		</Item>
		<Item Name="PDU Size --ring.ctl" Type="VI" URL="../PDU Size --ring.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
	</Item>
	<Item Name="SubVIs" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="COTP" Type="Folder">
			<Item Name="COTP - Add Header.vi" Type="VI" URL="../SubVIs/COTP/COTP - Add Header.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$O!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!N!"1!%1HFU:1!!%E"!!!(`````!!5%6&amp;"%61!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!51%!!!@````]!"1:4.S"12&amp;5!!&amp;1!]!!-!!-!"!!'!!1!"!!%!!1!"!!(!!1!#!!%!A!!?!!!$1A!!!!!!!!*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!B!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
			</Item>
			<Item Name="COTP - Build Connection Request Packet.vi" Type="VI" URL="../SubVIs/COTP/COTP - Build Connection Request Packet.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;,!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!N!"1!%1HFU:1!!*%"!!!(`````!!581U^55#"$&lt;WZO:7.U;7^O)&amp;*F=86F=X1!+E"Q!"Y!!")15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=Q!!$&amp;-X4G6U1W^N)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!I1(!!(A!!%B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T!!!,5T&gt;/:82$&lt;WUA;7Y!91$Q!!Q!!Q!%!!9!"Q!%!!1!"!!%!!A!"!!%!!E#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!!A!!!U!!!!-!!!!!!!!!!!!!!%!#A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
			</Item>
			<Item Name="COTP - Called TSAP Parameter--constant.vi" Type="VI" URL="../SubVIs/COTP/COTP - Called TSAP Parameter--constant.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#*!!!!"!!%!!!!#U!&amp;!!2#?82F!!!?1%!!!@````]!!2"$4V21)%.B&lt;'RF:#"55U&amp;1!!"5!0!!$!!!!!!!!!!#!!!!!!!!!!!!!!!!!!!!!!)!!(A!!!!!!!!!!!!!!!!!!!E!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"!!-!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
			</Item>
			<Item Name="COTP - Called TSAP Parameter.vi" Type="VI" URL="../SubVIs/COTP/COTP - Called TSAP Parameter.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#H!!!!"A!%!!!!#U!&amp;!!2#?82F!!!?1%!!!@````]!!2"$4V21)%.B&lt;'RF:#"55U&amp;1!!!01!5!#&amp;*B9WMA4G]O!!!01!5!#&amp;.M&lt;X1A4G]O!!"5!0!!$!!!!!!!!!!#!!!!!!!!!!!!!!!!!!-!"!)!!(A!!!!!!!!!!!!!!!!!!!E!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!)!!!!#!!!!!!"!!5!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
			</Item>
			<Item Name="COTP - Calling TSAP Parameter--constant.vi" Type="VI" URL="../SubVIs/COTP/COTP - Calling TSAP Parameter--constant.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#*!!!!"!!%!!!!#U!&amp;!!2#?82F!!!?1%!!!@````]!!2&amp;$4V21)%.B&lt;'RJ&lt;G=A6&amp;."5!"5!0!!$!!!!!!!!!!#!!!!!!!!!!!!!!!!!!!!!!)!!(A!!!!!!!!!!!!!!!!!!!E!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"!!-!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
			</Item>
			<Item Name="COTP - Header for Connection Request--constant.vi" Type="VI" URL="../SubVIs/COTP/COTP - Header for Connection Request--constant.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#A!!!!"1!%!!!!'5!$!".$4V21)%.3)%BF972F=C"4;8JF!!N!"1!%1HFU:1!!(%"!!!(`````!!)/1U^55#"$5C"):7&amp;E:8)!!&amp;1!]!!-!!!!!!!"!!-!!!!!!!!!!!!!!!!!!!!!!A!!?!!!!!!!!!!!!!!*!!!!#1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%!"!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
			</Item>
			<Item Name="COTP - Header for Data Transfer--constant.vi" Type="VI" URL="../SubVIs/COTP/COTP - Header for Data Transfer--constant.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#A!!!!"1!%!!!!'5!$!".$4V21)%25)%BF972F=C"4;8JF!!N!"1!%1HFU:1!!(%"!!!(`````!!)/1U^55#"%6#"):7&amp;E:8)!!&amp;1!]!!-!!!!!!!"!!-!!!!!!!!!!!!!!!!!!!!!!A!!?!!!!!!!!!!!!!!*!!!!#1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%!"!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
			</Item>
			<Item Name="COTP - Read-Write Data.vi" Type="VI" URL="../SubVIs/COTP/COTP - Read-Write Data.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;*!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!N!"1!%1HFU:1!!'%"!!!(`````!!5+5T=A5%26)'^V&gt;!!!*%"Q!"Y!!")15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=Q!!"F-X)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71%!!!@````]!"1F4.S"12&amp;5A;7Y!)E"Q!"Y!!")15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=Q!!"6-X)'FO!'%!]!!-!!-!"!!'!!=!"!!%!!1!"!!)!!1!#1!+!Q!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!B!!!!!+!!!.!!!!$!!!!!!!!!!!!!!"!!M!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
			<Item Name="COTP - Response Header Mismatch--error.vi" Type="VI" URL="../SubVIs/COTP/COTP - Response Header Mismatch--error.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#&gt;!!!!"A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;1!]!!-!!-!"!!%!!1!"!!%!!1!"!!%!!1!"!!%!A!!?!!!#1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%!"1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">276836864</Property>
			</Item>
			<Item Name="COTP - TPDU Size Parameter--constant.vi" Type="VI" URL="../SubVIs/COTP/COTP - TPDU Size Parameter--constant.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#(!!!!"!!%!!!!#U!&amp;!!2#?82F!!!=1%!!!@````]!!1Z$4V21)&amp;212&amp;5A5WF[:1!!6!$Q!!Q!!!!!!!!!!A!!!!!!!!!!!!!!!!!!!!!#!!"Y!!!!!!!!!!!!!!!!!!!*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!$!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
			</Item>
			<Item Name="COTP - Validate Response Header.vi" Type="VI" URL="../SubVIs/COTP/COTP - Validate Response Header.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$O!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!N!"1!%1HFU:1!!&amp;%"!!!(`````!!5'5T=A5%26!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"*!1!!"`````Q!&amp;"&amp;212&amp;5!!&amp;1!]!!-!!-!"!!%!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!!!!!!#1!!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!)1!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
		</Item>
		<Item Name="S7COMM" Type="Folder">
			<Item Name="S7COMM - Add JOB Header to Params and Data.vi" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Add JOB Header to Params and Data.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$#!!!!"Q!%!!!!#U!&amp;!!2#?82F!!!51%!!!@````]!!1:4.S"12&amp;5!!"2!1!!"`````Q!""V-X)%2B&gt;'%!'E"!!!(`````!!%.5T=A5'&amp;S97VF&gt;'6S=Q!61!9!$V.F=86F&lt;G.F)%ZV&lt;7*F=A"5!0!!$!!!!!!!!!!#!!!!!!!!!!!!!!!$!!1!"1)!!(A!!!!!!!!!!!!!!!!!!!E!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!B!!!!)1!!!!#!!!!!!"!!9!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342714370</Property>
			</Item>
			<Item Name="S7COMM - Build Data Array.vi" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Build Data Array.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%=!!!!#!!%!!!!$5!&amp;!!&gt;/&gt;7VF=GFD!"2!1!!"`````Q!""V-X)%2B&gt;'%!%5!'!!N%982B)%RF&lt;G&gt;U;!!&amp;!!5!!"*!1!!"`````Q!%"%2B&gt;'%!!(-!]1!!!!!!!!!#%&amp;-X4G6U1W^N,GRW9WRB=X-55T&gt;@6G&amp;S;7&amp;C&lt;'6@6(FQ:3ZD&gt;'Q!25!6!!E$,3UN!U**6!2#762&amp;"%.)16)%6U^32!.*4F1&amp;2&amp;&gt;05E1%2%F/6!2325&amp;-!!!.6G&amp;S;7&amp;C&lt;'5A6(FQ:1"5!0!!$!!!!!!!!A!$!!!!!!!!!!!!!!!!!!5!"A)!!(A!!!!!!!!!!!!!$1I!!!E!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%+!!!!#!!!!!!"!!=!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821058</Property>
			</Item>
			<Item Name="S7COMM - Build Params - any-type Addressing.vi" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Build Params - any-type Addressing.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)7!!!!$!!%!!!!#U!&amp;!!2#?82F!!!71%!!!@````]!!1F4.S"198*B&lt;8-!%U!(!!V4&gt;'&amp;S&gt;#"":'2S:8.T!"&amp;!"1!+1GFU)%^G:H.F&gt;!!!?1$RX'1CA1!!!!)15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=R*4.V^.:7VP=HF@18*F93ZD&gt;'Q!45!6!!='5#""=G6B"EEA18*F91:0)%&amp;S:7%'43""=G6B"U2#)%&amp;S:7%(1W^V&lt;H2F=A65;7VF=A!/5T&gt;@476N&lt;X*Z8U&amp;S:7%!!&amp;9!]1!!!!!!!!!#%&amp;-X4G6U1W^N,GRW9WRB=X-B5T&gt;$4UV.)#UA2H6O9X2J&lt;WYA1W^E:8-N,8*J&lt;G=O9X2M!"N!"1!15T&gt;@2H6O9X2J&lt;WZ@1W^E:1!!%5!&amp;!!J*&gt;'6N)%.P&gt;7ZU!!!01!9!#52#)%ZV&lt;7*F=A!,1!9!"5.P&gt;7ZU!(=!]1!!!!!!!!!#%&amp;-X4G6U1W^N,GRW9WRB=X-55T&gt;@6G&amp;S;7&amp;C&lt;'6@6(FQ:3ZD&gt;'Q!35!6!!E$,3UN!U**6!2#762&amp;"%.)16)%6U^32!.*4F1&amp;2&amp;&gt;05E1%2%F/6!2325&amp;-!!!15T&gt;@6G&amp;S;7&amp;C&lt;'6@6(FQ:1!!6!$Q!!Q!!!!!!!!!!A!$!!1!"1!'!!=!#!!*!!I#!!"Y!!!!!!!!!!!!!!!!!!!*!!!!%A!!!!!!!!!1!!!!#!!!!"!!!!!!!!!!%!!!!"!!!!!!!1!,!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
			</Item>
			<Item Name="S7COMM - Create JOB Message Header.vi" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Create JOB Message Header.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$%!!!!"Q!%!!!!"1!&amp;!!!;1%!!!@````]!!1V4.S"+4U)A3'6B:'6S!"6!"A!/5T=A2'&amp;U93"-:7ZH&gt;'A!!"N!"A!55T=A5'&amp;S97VF&gt;'6S=S"-:7ZH&gt;'A!!"6!"A!05W6R&gt;76O9W5A4H6N9G6S!&amp;1!]!!-!!!!!!!!!!)!!!!!!!!!!!!!!!-!"!!&amp;!A!!?!!!!!!!!!!!!!!!!!!!#1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!A!!!!)!!!!!!%!"A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">276836864</Property>
			</Item>
			<Item Name="S7COMM - DTL to Timestamp.vi" Type="VI" URL="../SubVIs/S7COMM/S7COMM - DTL to Timestamp.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'R!!!!&amp;!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!&amp;E!B%5FO&gt;G&amp;M;71A6'FN:8.U97VQ!!N!"A!%776B=A!!#U!&amp;!!6.&lt;WZU;!!*1!5!!U2B?1!,1!5!"%BP&gt;8)!!!V!"1!'47FO&gt;82F!!!.1!5!"F.F9W^O:!!!%5!(!!J/97ZP=W6D&lt;WZE!!!.1!5!"V&gt;F:7NE98E!51$R!!!!!!!!!!)15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=RF4.U.045UA,3"%6%QN,7.M&gt;8.U:8)O9X2M!"Z!5!!)!!5!"A!(!!A!#1!+!!M!$!.%6%Q!%E"5!!9+6'FN:3"4&gt;'&amp;N=!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!"1!&amp;!!!51%!!!@````]!%1:4.S"%6%Q!!&amp;1!]!!-!!-!"!!.!!Y!$Q!0!!]!$Q!1!!]!$Q!3!A!!?!!!$1A!!!E!!!!*!!!!#1!!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!)1!!!!!!%!%Q!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
			</Item>
			<Item Name="S7COMM - DTL--cluster.ctl" Type="VI" URL="../SubVIs/S7COMM/S7COMM - DTL--cluster.ctl">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$(!!!!#1!,1!9!"&amp;FF98)!!!N!"1!&amp;47^O&gt;'A!#5!&amp;!!.%98E!#U!&amp;!!2)&lt;X6S!!!.1!5!"EVJ&lt;H6U:1!!$5!&amp;!!:4:7.P&lt;G1!!"&amp;!"Q!+4G&amp;O&lt;X.F9W^O:!!!$5!&amp;!!&gt;8:76L:'&amp;Z!&amp;U!]1!!!!!!!!!#%&amp;-X4G6U1W^N,GRW9WRB=X-:5T&gt;$4UV.)#UA2&amp;2-,3VD&lt;(6T&gt;'6S,G.U&lt;!!K1&amp;!!#!!!!!%!!A!$!!1!"1!'!!=/5T=A2'&amp;U:3!G)&amp;2J&lt;75!!!%!#!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
			</Item>
			<Item Name="S7COMM - Evaluate Message Type.vi" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Evaluate Message Type.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%3!!!!#!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!61$R!!!!!!!!!!)15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=S"4.U.045UA,3".:8.T97&gt;F)&amp;2Z='6T,3VS;7ZH,G.U&lt;!!&lt;1!5!%&amp;"-1S".:8.T97&gt;F)&amp;2Z='5!!&amp;1!]!!-!!-!"!!%!!1!"!!%!!1!"!!&amp;!!1!"!!'!A!!?!!!$1A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!1!!!!!!%!"Q!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">276836864</Property>
			</Item>
			<Item Name="S7COMM - Evaluate Protocol Header.vi" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Evaluate Protocol Header.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$L!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!'5!&amp;!"*14%-A5G6T='^O=W5A6G&amp;M&gt;75!!"6!"1!/5T=A5(*P&gt;'^D&lt;WQA351!!&amp;1!]!!-!!-!"!!%!!1!"!!%!!1!"!!&amp;!!1!"A!(!A!!?!!!$1A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!"!!!!!1!!!!!!%!#!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">276836864</Property>
			</Item>
			<Item Name="S7COMM - Evaluate Response Error Code.vi" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Evaluate Response Error Code.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$/!!!!#!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%5!'!!J&amp;=H*P=C"$&lt;W2F!!"5!0!!$!!$!!1!"!!%!!1!"!!%!!1!"1!%!!1!"A)!!(A!!!U)!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!!!!"!!=!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">276836864</Property>
			</Item>
			<Item Name="S7COMM - Evaluate Response Item Return Code.vi" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Evaluate Response Item Return Code.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$/!!!!#!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%5!&amp;!!N3:82V=GYA1W^E:1"5!0!!$!!$!!1!"!!%!!1!"!!%!!1!"1!%!!1!"A)!!(A!!!U)!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!!!!"!!=!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
			</Item>
			<Item Name="S7COMM - Evaluate Sequence Number.vi" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Evaluate Sequence Number.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$J!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!'5!'!".14%-A5G6T='^O=W5A5W6R)%ZP!".!"A!-5W6O&gt;#"4:8%A4G]O!!"5!0!!$!!$!!1!"!!%!!1!"!!%!!1!"1!%!!9!"Q)!!(A!!!U)!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!1!!!!%!!!!!!"!!A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">276836864</Property>
			</Item>
			<Item Name="S7COMM - Evaluate Setup Communication Response.vi" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Evaluate Setup Communication Response.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%\!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#J!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!R4.UZF&gt;%.P&lt;3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!#U!&amp;!!2#?82F!!!51%!!!@````]!"Q:4.S"12&amp;5!!#B!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!N4.UZF&gt;%.P&lt;3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!A!#1-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!)1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!+!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342714386</Property>
			</Item>
			<Item Name="S7COMM - Format Read Request.vi" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Format Read Request.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)%!!!!$!!%!!!!#U!&amp;!!2#?82F!!!91%!!!@````]!!1N4.S"12&amp;5A2'&amp;U91!E1(!!(A!!%B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T!!!'5T=A&lt;X6U!!!21!5!#E*J&gt;#"0:G:T:81!!"&amp;!"A!,5G6B:#"-:7ZH&gt;'A!=Q$R!!!!!!!!!!)15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=R24.V^798*J97*M:6^5?8"F,G.U&lt;!"&amp;1"5!#1-N,3U$1EF5"%*:6%5%1UB"5A284V*%!UF/6!6%6U^32!2%35Z5"&amp;*&amp;15Q!!!V798*J97*M:3"5?8"F!".!"A!-2'&amp;U93"#&lt;'^D;S!D!!!41!=!$6.U98*U)%&amp;E:(*F=X-!&gt;1$RX'1CA1!!!!)15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=R*4.V^.:7VP=HF@18*F93ZD&gt;'Q!35!6!!='5#""=G6B"EEA18*F91:0)%&amp;S:7%'43""=G6B"U2#)%&amp;S:7%(1W^V&lt;H2F=A65;7VF=A!,476N&lt;X*Z)%&amp;S:7%!)%"Q!"Y!!")15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=Q!!!F-X!!"B!0!!$!!!!!!!!A!$!!1!!!!&amp;!!9!"Q!)!!E!#A)!!(A!!!!!!!!!!!!!#1!!!!U,!!!!!!!!!!!!!"!!!!!1!!!!!!!!!")!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!,!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
			</Item>
			<Item Name="S7COMM - Format Write Request.vi" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Format Write Request.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)-!!!!$1!%!!!!$5!&amp;!!&gt;/&gt;7VF=GFD!"B!1!!"`````Q!"#V-X)&amp;"%63"%982B!#2!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!:4.S"P&gt;81!!"&amp;!"1!+1GFU)%^G:H.F&gt;!!!"1!&amp;!!!31%!!!@````]!"12%982B!!"T!0%!!!!!!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T&amp;&amp;-X8V:B=GFB9GRF8V2Z='5O9X2M!%6!&amp;1!*!SUN,1.#361%1FF5212$3%&amp;3"&amp;&gt;05E1$35Z5"5284V*%"%2*4F1%5E6"4!!!$6:B=GFB9GRF)&amp;2Z='5!%U!'!!R%982B)%*M&lt;W.L)#-!!".!"Q!.5X2B=H1A172E=G6T=Q"V!0(=:#+"!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T%F-X8UVF&lt;7^S?6^"=G6B,G.U&lt;!"*1"5!"Q:1)%&amp;S:7%'33""=G6B"E]A18*F91:.)%&amp;S:7%(2%)A18*F91&gt;$&lt;X6O&gt;'6S"62J&lt;76S!!N.:7VP=HEA18*F91!A1(!!(A!!%B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T!!!#5T=!!'%!]!!-!!!!!!!#!!-!"!!!!!9!"Q!)!!E!#A!,!A!!?!!!!!!!!!!!!!!*!!!!$1M!!!!!!!!!!!!"%A!!!"!!!!!!!!!!%A!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!Q!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821058</Property>
			</Item>
			<Item Name="S7COMM - Function Codes--ring.ctl" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Function Codes--ring.ctl">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"=!!!!!1"5!0%!!!!!!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T)6-X1U^.43!N)%:V&lt;G.U;7^O)%.P:'6T,3VS;7ZH,G.U&lt;!!:1!5!$E:V&lt;G.U;7^O)%.P:'6T!!!"!!!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
			</Item>
			<Item Name="S7COMM - Get Size Factor from Transport Data Type.vi" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Get Size Factor from Transport Data Type.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$0!!!!"!!%!!!!%5!'!!N4;8JF)%:B9X2P=A"?!0%!!!!!!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T*V-X1U^.43!N)&amp;2S97ZT='^S&gt;#"%982B)&amp;2Z='6T,3VS;7ZH,G.U&lt;!!&gt;1!5!%V2S97ZT='^S&gt;#"%982B)&amp;2Z='5!6!$Q!!Q!!!!!!!!!!1!!!!!!!!!!!!!!!!!!!!)#!!"Y!!!!!!!!!!!!!!!!!!!*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"!!!!!!!1!$!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">276836864</Property>
			</Item>
			<Item Name="S7COMM - Header - Protocol ID--constant.vi" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Header - Protocol ID--constant.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"R!!!!!Q!%!!!!%5!&amp;!!N1=G^U&lt;W.P&lt;#"*2!"5!0!!$!!!!!!!!!!"!!!!!!!!!!!!!!!!!!!!!!)!!(A!!!!!!!!!!!!!!!!!!!E!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"!!)!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">276836864</Property>
			</Item>
			<Item Name="S7COMM - Memory Area Values.vi" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Memory Area Values.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$S!!!!"!!%!!!!&amp;U!&amp;!"&amp;.:7VP=HEA18*F93"797RV:1"\!0(=:#+"!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T%F-X8UVF&lt;7^S?6^"=G6B,G.U&lt;!"01"5!"Q:1)%&amp;S:7%'33""=G6B"E]A18*F91:.)%&amp;S:7%(2%)A18*F91&gt;$&lt;X6O&gt;'6S"62J&lt;76S!"".:7VP=HEA18*F93"&amp;&lt;H6N!!"5!0!!$!!!!!!!!!!"!!!!!!!!!!!!!!!!!!!!!A)!!(A!!!!!!!!!!!!!!!!!!!E!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%!!!!!!"!!-!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">276836864</Property>
			</Item>
			<Item Name="S7COMM - Message Types--ring.ctl" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Message Types--ring.ctl">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":!!!!!1"2!0%!!!!!!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T)&amp;-X1U^.43!N)%VF=X.B:W5A6(FQ:8-N,8*J&lt;G=O9X2M!"&gt;!"1!.476T=W&amp;H:3"5?8"F=Q!"!!!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
			</Item>
			<Item Name="S7COMM - Parse Response - Decode Data Item.vi" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Parse Response - Decode Data Item.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!';!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!5!"1!!(E"!!!(`````!!515G6B:#"*&gt;'6N)#UA2'&amp;U91!!#U!&amp;!!2#?82F!!!?1%!!!@````]!"R&amp;3:8.Q&lt;WZT:3"%982B)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!(-!]1!!!!!!!!!#%&amp;-X4G6U1W^N,GRW9WRB=X-55T&gt;@6G&amp;S;7&amp;C&lt;'6@6(FQ:3ZD&gt;'Q!25!6!!E$,3UN!U**6!2#762&amp;"%.)16)%6U^32!.*4F1&amp;2&amp;&gt;05E1%2%F/6!2325&amp;-!!!.6G&amp;S;7&amp;C&lt;'5A6(FQ:1!?1%!!!@````]!"R"3:8.Q&lt;WZT:3"%982B)'FO!!"5!0!!$!!$!!1!"A!)!!1!"!!%!!1!#1!+!!1!#Q-!!(A!!!U)!!!!!!!!#1!!!!E!!!!!!!!!!!!!!!!!!!!!!!!!#A!!!"!!!!!!!!!#%!!!!!!"!!Q!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710290</Property>
			</Item>
			<Item Name="S7COMM - Parse Response - Decode Read Item - Split Data Array.vi" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Parse Response - Decode Read Item - Split Data Array.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;R!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!8A$R!!!!!!!!!!)15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=S&gt;4.U.045UA,3"5=G&amp;O=X"P=H1A2'&amp;U93"5?8"F=SUN=GFO:SZD&gt;'Q!(5!&amp;!".5=G&amp;O=X"P=H1A2'&amp;U93"5?8"F!!N!"A!&amp;1W^V&lt;H1!#U!&amp;!!2#?82F!!!C1%!!!@````]!"B63:8.U)'^G)&amp;*F=X"P&lt;H.F)%2B&gt;'%!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!(E"!!!(`````!!915G6T='^O=W5A2'&amp;U93"J&lt;A!!6!$Q!!Q!!Q!%!!5!"Q!)!!A!#!!)!!E!#!!)!!I#!!"Y!!!.#!!!#1!!!!E!!!!*!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!B!!!!!!!1!,!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
			</Item>
			<Item Name="S7COMM - Parse Response - Evaluate Header.vi" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Parse Response - Evaluate Header.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;M!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!&amp;5!'!!^4:8&amp;V:7ZD:3"/&gt;7VC:8)!#U!&amp;!!2#?82F!!!C1%!!!@````]!"264.U.045UA5'&amp;S97UA97ZE)%2B&gt;'%!*%"Q!"Y!!")15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=Q!!"F-X)'^V&gt;!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!(%"!!!(`````!!5/5T=A5%26)%2B&gt;'%A;7Y!!#"!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!*4.Q!!91$Q!!Q!!Q!%!!9!"Q!)!!A!#!!)!!E!#!!+!!M#!!"Y!!!.#!!!#1!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!#%!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!$!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
			</Item>
			<Item Name="S7COMM - Parse Response - Evaluate Params.vi" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Parse Response - Evaluate Params.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;H!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!%5!&amp;!!J*&gt;'6N)%.P&gt;7ZU!!"3!0%!!!!!!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T)6-X1U^.43!N)%:V&lt;G.U;7^O)%.P:'6T,3VS;7ZH,G.U&lt;!!81!5!$5:V&lt;G.U;7^O)%.P:'5!#U!&amp;!!2#?82F!!!91%!!!@````]!"AN4.U.045UA2'&amp;U91!%!!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!E1%!!!@````]!"B:4.U.045UA5'&amp;S97VT)'&amp;O:#"%982B!!"5!0!!$!!$!!1!"1!(!!A!#!!)!!A!#1!)!!A!#A)!!(A!!!U)!!!*!!!!#1!!!!E!!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!#%!!!!!!"!!M!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
			</Item>
			<Item Name="S7COMM - Parse Response.vi" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Parse Response.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(;!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!&amp;5!'!!^4:8&amp;V:7ZD:3"/&gt;7VC:8)!"1!&amp;!!!=1%!!!P``````````!!5+2'&amp;U93""=H*B?1!!*%"Q!"Y!!")15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=Q!!"F-X)'^V&gt;!!!"!!!!(-!]1!!!!!!!!!#%&amp;-X4G6U1W^N,GRW9WRB=X-55T&gt;@6G&amp;S;7&amp;C&lt;'6@6(FQ:3ZD&gt;'Q!25!6!!E$,3UN!U**6!2#762&amp;"%.)16)%6U^32!.*4F1&amp;2&amp;&gt;05E1%2%F/6!2325&amp;-!!!.6G&amp;S;7&amp;C&lt;'5A6(FQ:1!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!N!"1!%1HFU:1!!'%"!!!(`````!!M,5T=A5%26)%2B&gt;'%!)%"Q!"Y!!")15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=Q!!!F-X!!"B!0!!$!!$!!1!"A!(!!A!#!!)!!E!#A!)!!Q!$1-!!(A!!!U)!!!*!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!1!!!!#A!!!!!!!!)1!!!!#!!!$1!!!!Q!!!!!!!!!!!!!!1!/!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710290</Property>
			</Item>
			<Item Name="S7COMM - Response Error Code-error.vi" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Response Error Code-error.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#&gt;!!!!"A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;1!]!!-!!-!"!!%!!1!"!!%!!1!"!!%!!1!"!!%!A!!?!!!#1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%!"1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">276836864</Property>
			</Item>
			<Item Name="S7COMM - Response Error Codes--ring.ctl" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Response Error Codes--ring.ctl">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;+!!!!!1&amp;#!0%!!!!!!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T*V-X1U^.43!N)&amp;*F=X"P&lt;H.F)%6S=G^S)%.P:'6T,3VS;7ZH,G.U&lt;!%"1"9!#!^3:8.F=H:F:#!I-(AQ-#E63'&amp;S:(&gt;B=G5A2G&amp;V&lt;(1A+$"Y-$%J(E^C;G6D&gt;#""9W.F=X-A6GFP&lt;'&amp;U;7^O)#AQ?$!T+2N":'2S:8.T)%^V&gt;#"P:C"397ZH:3!I-(AQ.3E=67ZT&gt;8"Q&lt;X*U:71A2'&amp;U93"5?8"F)#AQ?$!W+3Z*&lt;G.P&lt;H.J=X2F&lt;H1A2'&amp;U93"5?8"F)#BT;8JF)'VJ=WVB&gt;'.I0SEA+$"Y-$=J(%^C;G6D&gt;#"E&lt;W6T)'ZP&gt;#"F?'FT&gt;#!I-(AQ13E/5X6D9W6T=S!I-(B'2CE!!"J3:8.Q&lt;WZT:3"*&gt;'6N)&amp;*F&gt;(6S&lt;C"$&lt;W2F=Q!!!1!!!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1074278912</Property>
			</Item>
			<Item Name="S7COMM - Response Item Return Code-error.vi" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Response Item Return Code-error.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#&gt;!!!!"A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;1!]!!-!!-!"!!%!!1!"!!%!!1!"!!%!!1!"!!%!A!!?!!!#1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%!"1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">276836864</Property>
			</Item>
			<Item Name="S7COMM - Response Item Return Codes--ring.ctl" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Response Item Return Codes--ring.ctl">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;1!!!!!1&amp;)!0%!!!!!!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T,6-X1U^.43!N)&amp;*F=X"P&lt;H.F)%FU:7UA5G6U&gt;8*O)%.P:'6T,3VS;7ZH,G.U&lt;!%"1"9!#!^3:8.F=H:F:#!I-(AQ-#E63'&amp;S:(&gt;B=G5A2G&amp;V&lt;(1A+$"Y-$%J(E^C;G6D&gt;#""9W.F=X-A6GFP&lt;'&amp;U;7^O)#AQ?$!T+2N":'2S:8.T)%^V&gt;#"P:C"397ZH:3!I-(AQ.3E=67ZT&gt;8"Q&lt;X*U:71A2'&amp;U93"5?8"F)#AQ?$!W+3Z*&lt;G.P&lt;H.J=X2F&lt;H1A2'&amp;U93"5?8"F)#BT;8JF)'VJ=WVB&gt;'.I0SEA+$"Y-$=J(%^C;G6D&gt;#"E&lt;W6T)'ZP&gt;#"F?'FT&gt;#!I-(AQ13E/5X6D9W6T=S!I-(B'2CE!!"J3:8.Q&lt;WZT:3"*&gt;'6N)&amp;*F&gt;(6S&lt;C"$&lt;W2F=Q!!!1!!!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
			</Item>
			<Item Name="S7COMM - Response Message Type Mismatch-error.vi" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Response Message Type Mismatch-error.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#&gt;!!!!"A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;1!]!!-!!-!"!!%!!1!"!!%!!1!"!!%!!1!"!!%!A!!?!!!#1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%!"1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">276836864</Property>
			</Item>
			<Item Name="S7COMM - Response Protocol Header Mismatch-error.vi" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Response Protocol Header Mismatch-error.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#&gt;!!!!"A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;1!]!!-!!-!"!!%!!1!"!!%!!1!"!!%!!1!"!!%!A!!?!!!#1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%!"1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">276836864</Property>
			</Item>
			<Item Name="S7COMM - Response Sequence Number Mismatch-error.vi" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Response Sequence Number Mismatch-error.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#&gt;!!!!"A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;1!]!!-!!-!"!!%!!1!"!!%!!1!"!!%!!1!"!!%!A!!?!!!#1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%!"1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">276836864</Property>
			</Item>
			<Item Name="S7COMM - Set PDU Size.vi" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Set PDU Size.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#Q!!!!"A!%!!!!#U!&amp;!!2#?82F!!!91%!!!@````]!!1J4.S"12&amp;5A&lt;X6U!!!81!9!%&amp;"%63"4;8JF)#B#?82F=SE!!":!1!!"`````Q!"#6-X)&amp;"%63"J&lt;A"5!0!!$!!!!!!!!!!#!!!!!!!!!!!!!!!!!!-!"!)!!(A!!!!!!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!!!#%A!!!!!"!!5!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
			</Item>
			<Item Name="S7COMM - Setup Communication Parameters--constant.vi" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Setup Communication Parameters--constant.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#@!!!!"!!%!!!!#U!&amp;!!2#?82F!!!U1%!!!@````]!!3:4.V"%63!C5W6U&gt;8!A1W^N&lt;86O;7.B&gt;'FP&lt;C)A5'&amp;S97VF&gt;'6S=Q!!6!$Q!!Q!!!!!!!!!!A!!!!!!!!!!!!!!!!!!!!!#!!"Y!!!!!!!!!!!!!!!!!!!*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!$!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
			</Item>
			<Item Name="S7COMM - Setup Communication.vi" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Setup Communication.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%^!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!"1!(4H6N:8*J9Q!51%!!!@````]!"1:4.S"12&amp;5!!#J!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!R4.UZF&gt;%.P&lt;3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!+%"Q!"Y!!")15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=Q!!#V-X4G6U1W^N)'FO!'%!]!!-!!-!"!!'!!=!"!!%!!1!"!!)!!1!"!!*!Q!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!I!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342714386</Property>
			</Item>
			<Item Name="S7COMM - Siemens Date &amp; Time to Timestamp.vi" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Siemens Date &amp; Time to Timestamp.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'\!!!!&amp;!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!&amp;E!B%5FO&gt;G&amp;M;71A6'FN:8.U97VQ!!N!"A!%776B=A!!#U!&amp;!!6.&lt;WZU;!!*1!5!!U2B?1!,1!5!"%BP&gt;8)!!!V!"1!'47FO&gt;82F!!!.1!5!"F.F9W^O:!!!%5!(!!J/97ZP=W6D&lt;WZE!!!.1!5!"V&gt;F:7NE98E!51$R!!!!!!!!!!)15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=RF4.U.045UA,3"%6%QN,7.M&gt;8.U:8)O9X2M!"Z!5!!)!!5!"A!(!!A!#1!+!!M!$!*%6!!!%E"5!!9+6'FN:3"4&gt;'&amp;N=!!!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!"1!&amp;!!!?1%!!!@````]!%2"4.S"%162&amp;8U&amp;/2&amp;^535V&amp;!!"5!0!!$!!$!!1!$1!/!!]!$Q!0!!]!%!!0!!]!%A)!!(A!!!U)!!!*!!!!#1!!!!E!!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!"%A!!!!!"!"-!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
			</Item>
			<Item Name="S7COMM - Siemens Date &amp; Time--cluster.ctl" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Siemens Date &amp; Time--cluster.ctl">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$8!!!!#1!,1!-!"&amp;FF98)!!!N!"1!&amp;47^O&gt;'A!#5!&amp;!!.%98E!#U!&amp;!!2)&lt;X6S!!!.1!5!"EVJ&lt;H6U:1!!$5!&amp;!!:4:7.P&lt;G1!!"&amp;!"Q!,47FM&lt;'FT:7.P&lt;G1!$5!&amp;!!&gt;8:76L:'&amp;Z!'U!]1!!!!!!!!!#%&amp;-X4G6U1W^N,GRW9WRB=X-J5T&gt;$4UV.)#UA5WFF&lt;76O=S"%982F)#9A6'FN:3UN9WRV=X2F=CZD&gt;'Q!+E"1!!A!!!!"!!)!!Q!%!!5!"A!($F-X)%2B&gt;'5A*C"5;7VF!!!"!!A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1074278912</Property>
			</Item>
			<Item Name="S7COMM - Timestamp to DTL.vi" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Timestamp to DTL.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%_!!!!$A!%!!!!#U!'!!2::7&amp;S!!!,1!5!"5VP&lt;H2I!!F!"1!$2'&amp;Z!!N!"1!%3'^V=A!!$5!&amp;!!:.;7ZV&gt;'5!!!V!"1!'5W6D&lt;WZE!!!21!=!#EZB&lt;G^T:7.P&lt;G1!!!V!"1!(6W6F;W2B?1"2!0%!!!!!!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T'6-X1U^.43!N)%254#UN9WRV=X2F=CZD&gt;'Q!(E"1!!A!!1!#!!-!"!!&amp;!!9!"Q!)!U254!!&amp;!!5!!"2!1!!"`````Q!+"F-X)%254!!!%E"5!!9+6'FN:3"4&gt;'&amp;N=!!!6!$Q!!Q!!!!*!!!!#Q!!!!!!!!!!!!!!!!!!!!Q#!!"Y!!!!!!!!#1!!!!!!!!!*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"!!!!!!!1!.!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
			</Item>
			<Item Name="S7COMM - Timestamp to Siemens Date &amp; Time.vi" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Timestamp to Siemens Date &amp; Time.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'L!!!!%Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!#U!$!!2::7&amp;S!!!,1!5!"5VP&lt;H2I!!F!"1!$2'&amp;Z!!N!"1!%3'^V=A!!$5!&amp;!!:.;7ZV&gt;'5!!!V!"1!'5W6D&lt;WZE!!!21!=!#UVJ&lt;'RJ=W6D&lt;WZE!!V!"1!(6W6F;W2B?1"B!0%!!!!!!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T+6-X1U^.43!N)&amp;.J:7VF&lt;H-A2'&amp;U:3!G)&amp;2J&lt;75N,7.M&gt;8.U:8)O9X2M!"Z!5!!)!!1!"1!'!!=!#!!*!!I!#Q*%6!!!"!!!!!5!"1!!(E"!!!(`````!!Y15T=A2%&amp;526^"4E2@6%F.21!!&amp;E"1!!-!!!!"!!)):8*S&lt;X)A;7Y!!"*!6!!'#F2J&lt;75A5X2B&lt;8!!!&amp;1!]!!-!!-!$!!.!!]!$1!.!!U!$1!1!!U!$1!2!A!!?!!!$1A!!!E!!!!!!!!!#1!!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!1!!!!!!%!%A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
			</Item>
			<Item Name="S7COMM - Transport Data Types--ring.ctl" Type="VI" URL="../SubVIs/S7COMM/S7COMM - Transport Data Types--ring.ctl">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"=!!!!!1"5!0%!!!!!!!!!!B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T)V-X1U^.43!N)&amp;*F='RZ)%2B&gt;'%A6(FQ:8-N,8*J&lt;G=O9X2M!"&gt;!"1!.476T=W&amp;H:3"5?8"F=Q!"!!!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1074278912</Property>
			</Item>
		</Item>
		<Item Name="TCP" Type="Folder">
			<Item Name="TCP - Close Connection.vi" Type="VI" URL="../SubVIs/TCP/TCP - Close Connection.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%=!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#J!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!R4.UZF&gt;%.P&lt;3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!+%"Q!"Y!!")15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=Q!!#V-X4G6U1W^N)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="TCP - Open Connection.vi" Type="VI" URL="../SubVIs/TCP/TCP - Open Connection.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%=!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#J!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!R4.UZF&gt;%.P&lt;3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!+%"Q!"Y!!")15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=Q!!#V-X4G6U1W^N)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
			</Item>
		</Item>
		<Item Name="TPKT" Type="Folder">
			<Item Name="TPKT - Add Header.vi" Type="VI" URL="../SubVIs/TPKT/TPKT - Add Header.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$M!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!N!"1!%1HFU:1!!%E"!!!(`````!!5%6&amp;",6!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!31%!!!@````]!"1255%26!!"5!0!!$!!$!!1!"A!%!!1!"!!%!!1!"Q!%!!A!"!)!!(A!!!U)!!!!!!!!#1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!)1!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
			</Item>
			<Item Name="TPKT - Decode Header.vi" Type="VI" URL="../SubVIs/TPKT/TPKT - Decode Header.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$R!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"&amp;!"A!,6&amp;"%63"-:7ZH&gt;'A!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!,1!5!"%*Z&gt;'5!!"B!1!!"`````Q!(#V213V1A3'6B:'6S!&amp;1!]!!-!!-!"!!&amp;!!1!"!!%!!1!"!!'!!1!#!!%!A!!?!!!$1A!!!!!!!!*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!B!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821058</Property>
			</Item>
			<Item Name="TPKT - Get TDPU Length.vi" Type="VI" URL="../SubVIs/TPKT/TPKT - Get TDPU Length.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$R!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"&amp;!"A!,6&amp;"%63"-:7ZH&gt;'A!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!,1!5!"%*Z&gt;'5!!"B!1!!"`````Q!(#V213V1A3'6B:'6S!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!)!A!!?!!!$1A!!!!!!!!!!!!!#1!!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!)1!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821058</Property>
			</Item>
			<Item Name="TPKT - Header Bytes 1 and 2--constant.vi" Type="VI" URL="../SubVIs/TPKT/TPKT - Header Bytes 1 and 2--constant.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#4!!!!"!!%!!!!$U!&amp;!!B3:8.F=H:F:!!!*%"!!!(`````!!%86&amp;",6#"):7&amp;E:8)A1HFU:8-A-3!G)$)!6!$Q!!Q!!!!!!!!!!A!!!!!!!!!!!!!!!!!!!!!#!!"Y!!!!!!!!!!!!!!!!!!!*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!$!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
			</Item>
			<Item Name="TPKT - Read Packet.vi" Type="VI" URL="../SubVIs/TPKT/TPKT - Read Packet.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%F!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!5!"1!!%E"!!!(`````!!5%6&amp;"%61!!*%"Q!"Y!!")15T&gt;/:82$&lt;WUO&lt;(:D&lt;'&amp;T=Q!!"F-X)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!A1(!!(A!!%B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T!!!#5T=!!'%!]!!-!!-!"!!'!!=!"!!%!!1!"!!)!!1!"!!*!Q!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!+!!!.!!!!$!!!!!!!!!!!!!!"!!I!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710290</Property>
			</Item>
			<Item Name="TPKT - Response Header Mismatch--error.vi" Type="VI" URL="../SubVIs/TPKT/TPKT - Response Header Mismatch--error.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#&gt;!!!!"A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;1!]!!-!!-!"!!%!!1!"!!%!!1!"!!%!!1!"!!%!A!!?!!!#1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%!"1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
			</Item>
			<Item Name="TPKT - Validate Header Bytes.vi" Type="VI" URL="../SubVIs/TPKT/TPKT - Validate Header Bytes.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$A!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!#U!&amp;!!2#?82F!!!91%!!!@````]!"AN55%N5)%BF972F=A"5!0!!$!!$!!1!"!!%!!1!"!!%!!1!"1!%!!1!"Q)!!(A!!!U)!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!#%!!!!!!"!!A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821058</Property>
			</Item>
			<Item Name="TPKT - Write Packet.vi" Type="VI" URL="../SubVIs/TPKT/TPKT - Write Packet.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%B!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#2!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!:4.S"P&gt;81!!":!5!!$!!!!!1!##'6S=G^S)'FO!!!,1!5!"%*Z&gt;'5!!"*!1!!"`````Q!("&amp;212&amp;5!!#"!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!*4.Q!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!)!!E$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!#%!!!!!I!!!U!!!!-!!!!!!!!!!!!!!%!#A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
		</Item>
		<Item Name="Connection - COPT Connection Request.vi" Type="VI" URL="../SubVIs/Connection - COPT Connection Request.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%\!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!"1!(4H6N:8*J9Q!31%!!!@````]!"1255%26!!!K1(!!(A!!%B"4.UZF&gt;%.P&lt;3ZM&gt;G.M98.T!!!-5T&gt;/:82$&lt;WUA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#B!=!!?!!!3%&amp;-X4G6U1W^N,GRW9WRB=X-!!!N4.UZF&gt;%.P&lt;3"J&lt;A"B!0!!$!!$!!1!"A!(!!1!"!!%!!1!#!!%!!1!#1-!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!+!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
	</Item>
</LVClass>
